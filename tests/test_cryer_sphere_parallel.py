import os
import pytest
import sys


@pytest.mark.parallel
def test_mpi():
    print("Running code")
    com = "mpirun -v -np 2 " + str(sys.executable) + " -m cryer_sphere.py"
    com = "cd examples/cryer_sphere && " + com + " && cd -"
    err = os.system(com)
    print(err)
    print("Done")
