#!/usr/bin/env python3

# Arrange
import akantu as aka
import numpy as np
import os
import sys
import pytest

sys.path.append(
    os.path.join(
        sys.path[0],
        "..",
        "examples",
        "analytical_solutions",
    )
)
from akantu_geomechanical_solver import AkantuGeomechanicalSolver
from akantu_geomechanical_solver import IterativeOptions
import analytical_solutions as an_sol


class FixedPressure(aka.DirichletFunctor):
    def __init__(self, value):
        super().__init__()
        self.value = value

    def __call__(self, node, flags, primal, coord):
        # sets the blocked dofs vector to true in the desired axis
        flags[0] = True
        primal[0] = self.value


def run_geomechanical_simulation(parameters):

    # material parameters
    poisson = (
        (3 * parameters["K"] - 2 * parameters["G"])
        / 2
        / (3 * parameters["K"] + parameters["G"])
    )  # [-] Poisson ratio
    E = (
        9 * parameters["K"] * parameters["G"] /
        (3 * parameters["K"] + parameters["G"])
    )  # [GPa] Young modulus
    one_over_M = 3.05e-2  # [1/GPa] Biot's modulus
    k = 1e-18  # [m2] permeability
    mu = 8.9e-13  # [GPa s] viscosity
    final_time = (
        parameters["ball_r"] * parameters["ball_r"] / parameters["c_v"]
    )  # [s] final time
    nb_timesteps = 50
    timestep = final_time / nb_timesteps
    times = np.linspace(0, final_time, nb_timesteps + 1)

    material_file = f"""
    material elastic [
        name = rock
        E   = {E}   # young's modulus [GPa]
        nu  = {poisson}      # poisson's ratio
    ]

    model heat_transfer_model [
            density = 1  #kg/m3
            conductivity = [[{k/mu}, 0, 0], \
                            [0,  {k/mu}, 0], \
                            [0,  0, {k/mu}]] # m2/(GPa s)
            capacity = {one_over_M}        # 1/GPa
    ]
    """
    # writing the material file
    open("material.dat", "w").write(material_file)

    # reading the material file
    material_file = "material.dat"
    aka.parseInput(material_file)

    # reading the mesh
    dim = 3
    mesh_file = "tests/sphere_coarse.msh"
    mesh = aka.Mesh(dim)
    mesh.read(mesh_file)

    mesh.distribute()

    rock_model = aka.SolidMechanicsModel(mesh)
    flow_model = aka.HeatTransferModel(mesh)

    rock_model.initFull(_analysis_method=aka._static)
    flow_model.initFull(_analysis_method=aka._implicit_dynamic)
    flow_model.setTimeStep(timestep)

    # set the displacement/Dirichlet boundary conditions
    rock_model.applyBC(aka.FixedValue(0.0, aka._x), "left")
    rock_model.applyBC(aka.FixedValue(0.0, aka._y), "back")
    rock_model.applyBC(aka.FixedValue(0.0, aka._z), "bottom")

    # set the force/Neumann boundary conditions
    surface_stress = np.eye(dim)
    surface_stress *= parameters["load"]
    rock_model.applyBC(aka.FromStress(surface_stress), "surface")

    flow_solver = flow_model.getNonLinearSolver()
    flow_solver.set("max_iterations", 10)
    flow_solver.set("threshold", 1e-7)
    flow_solver.set("convergence_type", aka.SolveConvergenceCriteria.residual)

    rock_solver = rock_model.getNonLinearSolver()
    rock_solver.set("max_iterations", 20)
    rock_solver.set("threshold", 1e-6)
    rock_solver.set("convergence_type", aka.SolveConvergenceCriteria.residual)

    it_options = IterativeOptions(
        pcg_maxiterations=50,
        newton_maxiterations=100,
        f_rtol=1e-9,
        f_rock_tol=1e-5,
        f_flow_tol=1e-8,
    )
    solver = AkantuGeomechanicalSolver(
        rock_model,
        flow_model,
        parameters["alpha"],
        dump_iterations=False,
        update_stiffness=False,
    )

    # prepare the initial guess arrays for the PCG solver
    nb_nodes = mesh.getNbNodes()
    delta_u_0 = np.zeros([nb_nodes, dim])
    delta_p_0 = np.zeros([nb_nodes, 1])

    # identify the central node for outputting the pressure
    central_node = 0
    central_node_found = False
    nodes = mesh.getNodes()
    node_flags = mesh.getNodesFlags()
    for node_nb in range(mesh.getNbNodes()):
        if np.allclose(nodes[node_nb], [0, 0, 0]) and node_flags[node_nb] == np.uint8(
            aka._normal
        ):
            central_node = node_nb
            central_node_found = True

    central_pressure = np.zeros(nb_timesteps + 1)

    # apply the analytical solution for pressure of the undrained response to the model
    p_0_an = an_sol.p_0_cryer(
        parameters["alpha"], parameters["K"], parameters["S_eps"], parameters["load"]
    )
    pressure = flow_model.getTemperature()
    blocked_dofs_p = flow_model.getBlockedDOFs()
    pressure.fill(p_0_an)
    blocked_dofs_p.fill(True)

    # solve the displacement part of the undrained response first
    cvged = solver.solve_step(delta_u_0, delta_p_0, it_options)
    if not cvged:
        raise Exception("Time step solver did not converge")

    if central_node_found:
        central_pressure[0] = pressure[central_node][0]

    # applying fixed pressure at the top
    blocked_dofs_p.fill(False)
    flow_model.applyBC(FixedPressure(0.0), "surface")

    for i in range(nb_timesteps):
        print(f"\nSolving time step {i}", flush=True)

        cvged = solver.solve_step(delta_u_0, delta_p_0, it_options)
        if not cvged:
            raise Exception("Time step solver did not converge")

        if central_node_found:
            central_pressure[i + 1] = pressure[central_node][0]

    # At the end of your function, return the results
    return central_pressure, times


def compute_analytical_solution(times, parameters):
    p_0_an = an_sol.p_0_cryer(
        parameters["alpha"], parameters["K"], parameters["S_eps"], parameters["load"]
    )  # duplicate from the other
    analytical_pressures = np.zeros(times.size)
    analytical_pressures[0] = p_0_an
    analytical_pressures[1:] = an_sol.p_cryer(
        parameters["alpha"],
        parameters["K"],
        parameters["S_eps"],
        parameters["G"],
        parameters["m_v"],
        p_0_an,
        times[1:],
        parameters["c_v"],
        parameters["ball_r"],
        100,
    )

    return analytical_pressures


@pytest.fixture
def test_parameters():
    parameters = {"alpha": 0.9, "K": 10, "G": 6, "load": 5, "ball_r": 1}

    # Add m_v to parameters
    parameters["m_v"] = 1 / (parameters["K"] + 4 / 3 * parameters["G"])

    # Add c_v to parameters
    one_over_M = 3.05e-2  # [1/GPa] Biot's modulus
    k = 1e-18  # [m2] permeability
    mu = 8.9e-13  # [GPa s] viscosity
    S = one_over_M + 3 * parameters["alpha"] ** 2 / (
        3 * parameters["K"] + 4 * parameters["G"]
    )  # storage coefficient
    parameters["c_v"] = (
        k / mu / S
    )  # generalized consolidation coefficient (Cheng's book, p.198)

    # Add S_eps to parameters
    beta_s = 1.0e-2  # [1/GPa] compressibility of solid particles
    beta_f = 4.4e-1  # [1/GPa] compressibility of the fluid
    # [1/GPa] drained storage coefficient (see Haagenson et al. 2020)
    phi_0 = 0.05  # [-] porosity
    parameters["S_eps"] = (parameters["alpha"] - phi_0) * \
        beta_s + phi_0 * beta_f
    return parameters


@pytest.mark.serial
def test_geomechanical_solver(test_parameters):

    # Arrange
    central_pressure, times = run_geomechanical_simulation(test_parameters)

    # Act: Calculate the analytical solution
    analytical_pressures = compute_analytical_solution(times, test_parameters)

    # Assert: Compare the numerical result with the analytical result
    tolerance = 5  # Relative tolerance, in percentage points
    # assert np.allclose(central_pressure, analytical_pressures, rtol=tolerance), \
    #     "Numerical solution differs from analytical solution beyond the acceptable tolerance" + \
    #     "Error is " + str(np.max(abs(central_pressure - analytical_pressures) / np.max(analytical_pressures) * 100))

    assert (
        np.max(
            abs(central_pressure - analytical_pressures)
            / np.max(analytical_pressures)
            * 100
        )
        < tolerance
    ), (
        "Numerical solution differs from analytical solution beyond the acceptable tolerance"
        + "Error is "
        + str(
            np.max(
                abs(central_pressure - analytical_pressures)
                / np.max(analytical_pressures)
                * 100
            )
        )
    )
