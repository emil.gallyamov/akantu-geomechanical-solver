.. akantu-geomechanical-solver documentation master file, created by
   sphinx-quickstart on Tue Jul  9 15:36:28 2024.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to akantu-geomechanical-solver's documentation!
=======================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   auto_source/akantu_geomechanical_solver/modules
   auto_source/akantu_geomechanical_solver/akantu_geomechanical_solver


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
