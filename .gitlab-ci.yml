stages:
  - build
  - test
  - deploy

package:
  stage: build
  image: python:latest
  script:
    - pip install build
    - python3 -m build
  artifacts:
    when: on_success
    paths:
      - dist/
    expire_in: 30 mins
  only:
    - tags

.build_image:
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  script:
    - /kaniko/executor
      --context "${CI_PROJECT_DIR}"
      --dockerfile "${CI_PROJECT_DIR}/docker/${DOCKERFILE}"
      --destination "${CI_REGISTRY_IMAGE}/${REPOSITORY}:${CI_COMMIT_REF_SLUG}"
      --build-arg CI_REGISTRY_IMAGE=$CI_REGISTRY_IMAGE

build_serial_gms_image:
  stage: build
  variables:
    DOCKERFILE: Dockerfile_gms_serial
    REPOSITORY: serial-gms-images
  extends: .build_image
  rules:
    - if: $CI_COMMIT_TAG
      when: never
    - if: '$CI_PIPELINE_SOURCE != "merge_request_event"'
      changes:
        - .gitlab-ci.yml
        - pyproject.toml
        - docker/Dockerfile_gms_serial

buid_parallel_akantu_image:
  stage: build
  variables:
    DOCKERFILE: Dockerfile_akantu_parallel
    REPOSITORY: parallel-akantu-images
  extends: .build_image
  rules:
    - if: $CI_COMMIT_TAG
      when: never
    - if: '$CI_PIPELINE_SOURCE != "merge_request_event"'
      changes:
        - .gitlab-ci.yml
        - installation.sh
        - docker/Dockerfile_akantu_parallel

build_parallel_gms_image:
  stage: build
  needs:
    - job: buid_parallel_akantu_image
      optional: true
  variables:
    DOCKERFILE: Dockerfile_gms_parallel
    REPOSITORY: parallel-gms-images
  extends: .build_image
  rules:
    - if: $CI_COMMIT_TAG
      when: never
    - if: '$CI_PIPELINE_SOURCE != "merge_request_event"'
      changes:
        - .gitlab-ci.yml
        - pyproject.toml
        - installation.sh
        - docker/Dockerfile_akantu_parallel
        - docker/Dockerfile_gms_parallel

test_serial:
  stage: test
  needs:
    - job: build_serial_gms_image
      optional: true
  rules:
    - if: $CI_COMMIT_BRANCH
      variables:
        IMAGE_TAG_SUFFIX: $CI_COMMIT_REF_SLUG
    - if: $CI_COMMIT_BRANCH == "" || $CI_COMMIT_BRANCH == null
      variables:
        IMAGE_TAG_SUFFIX: "main"
  image: "${CI_REGISTRY_IMAGE}/serial-gms-images:${IMAGE_TAG_SUFFIX}"
  script:
    - pip install .[test]
    - python3 -m pytest -m serial

test_parallel:
  stage: test
  needs:
    - job: build_parallel_gms_image
      optional: true
  rules:
    - if: $CI_COMMIT_BRANCH
      variables:
        IMAGE_TAG_SUFFIX: $CI_COMMIT_REF_SLUG
    - if: $CI_COMMIT_BRANCH == "" || $CI_COMMIT_BRANCH == null
      variables:
        IMAGE_TAG_SUFFIX: "main"
  image: "${CI_REGISTRY_IMAGE}/parallel-gms-images:${IMAGE_TAG_SUFFIX}"
  script:
    - pip install .[test]
    - python3 -m pytest -m parallel

publish_pypi:
  stage: deploy
  image: python:latest
  needs:
    - job: package
    - job: test_serial
  script:
    - pip install twine
    - export TWINE_USERNAME="__token__"
    - export TWINE_PASSWORD="$PYPI_TOKEN"
    - python3 -m twine upload dist/* --verbose
  only:
    - tags

pages:
  stage: deploy
  image: python:3.10-slim
  before_script:
    - apt-get update && apt-get install -y --no-install-recommends make g++ libopenmpi-dev openmpi-bin git
    - pip install .[serial,doc] --index-url https://gitlab.com/api/v4/projects/15663046/packages/pypi/simple
  script:
    - cd docs && make html SPHINXOPTS="-d _build/doctrees"
  after_script:
    - if [ "$CI_JOB_STATUS" == "canceled" ]; then exit 0; fi
    - mv docs/_build/html/ ./public/
  artifacts:
    paths:
    - public
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH

publish_parallel_dockerhub:
  stage: deploy
  variables:
    DOCKER_TAG: "${CI_COMMIT_TAG}-${CI_COMMIT_SHORT_SHA}"
    SOURCE_IMAGE: "${CI_REGISTRY_IMAGE}/parallel-gms-images:main"
    DESTINATION_IMAGE: "enacit4r/parallel-gms-images:${DOCKER_TAG}"
  image: docker:latest
  services:
    - docker:dind
  before_script:
    - echo "${CI_JOB_TOKEN}" | docker login -u "${CI_REGISTRY_USER}" --password-stdin "${CI_REGISTRY}"
    - docker pull "${SOURCE_IMAGE}"
    - echo "${DOCKER_HUB_PAT}" | docker login -u "enacit4r" --password-stdin
  script:
    - docker tag "${SOURCE_IMAGE}" "${DESTINATION_IMAGE}"
    - docker push "${DESTINATION_IMAGE}"
  only:
    - tags
