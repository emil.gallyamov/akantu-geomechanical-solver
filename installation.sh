# The SoftWare Hash IDentifier (SWISHID) swh:1:dir:3d006736ef1dc88b9305f234fd279f79253f6a22 corresponds to the last commit of
# the features/hydro_mechanical_coupling. Cooking it and downloading it via the Software Heritage API returns a directory that contains
# a single commit. In particular, CMake is not able to identify the version based on the git history as it should. The workaround is to
# tag the commit with the version number.

curl -X POST https://archive.softwareheritage.org/api/1/vault/git-bare/swh:1:dir:3d006736ef1dc88b9305f234fd279f79253f6a22
curl -L -o bundle.tar.gz https://archive.softwareheritage.org/api/1/vault/git-bare/swh:1:dir:3d006736ef1dc88b9305f234fd279f79253f6a22/raw
mkdir akantu.git
tar -xf bundle.tar.gz -C ./akantu.git --strip-components=1
git clone akantu.git
cd akantu
git tag v4.0.1

mkdir build
cd build

cmake .. -DAKANTU_IMPLICIT=TRUE -DAKANTU_PYTHON_INTERFACE=TRUE -DAKANTU_COHESIVE_ELEMENT=TRUE -DAKANTU_HEAT_TRANSFER=TRUE -DAKANTU_PARALLEL=TRUE -DAKANTU_SOLID_MECHANICS=TRUE -DCMAKE_BUILD_TYPE=Release
make
