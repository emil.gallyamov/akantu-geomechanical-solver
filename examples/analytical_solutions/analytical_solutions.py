#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 11 08:55:39 2022

Set of analytical solutions for the geomechanical problems
"""

__author__ = "Emil Gallyamov"
__credits__ = [
    "Emil Gallyamov <emil.gallyamov@epfl.ch>",
]
__copyright__ = (
    "Copyright (©) 2016-2021 EPFL (Ecole Polytechnique Fédérale"
    " de Lausanne) Laboratory (LSMS - Laboratoire de Simulation"
    " en Mécanique des Solides)"
)
__license__ = "LGPLv3"


import numpy as np
from scipy.optimize import fsolve
from scipy.special import exp1, erfc, ellipkinc, ellipeinc
import scipy
import mpmath


def p_0_terzaghi_haagenson(alpha: float, m_v: float, S_eps: float, load: float):
    """
    Solution for the pressure of the initial undrained response of a Terzaghi consolidation problem based on the paper of Haagenson et al. 2020

    Args:
        alpha (float): biot's coefficient
        m_v (float): confined compressibility of the porous medium
        S_eps (float): drained storage coefficient

    Returns:
        pressure (float): Pressure within the specimen during the undrained response.
    """
    return -alpha * m_v / (alpha**2 * m_v + S_eps) * load


def u_0_terzaghi_haagenson(m_v, alpha, S_eps, load, H, y):
    """
    Solution for the displacement of the initial undrained response of a Terzaghi consolidation problem based on the paper of Haagenson et al. 2020

    Args:
        alpha (float): biot's coefficient
        m_v (float): confined compressibility of the porous medium
        S_eps (float): drained storage coefficient
        y (float): distance of a point from the bottom of the specimen

    Returns:
        displacement (float): Vertical displacement of a point with coordinate y during the undrained response.
    """
    return (m_v - alpha**2 * m_v**2 / (alpha**2 * m_v + S_eps)) * load * (H - y) / H


def p_0_terzaghi(nu, G, S, load):
    """
    Solution for the displacement of the initial undrained response of a Terzaghi consolidation problem based on the book "Poroelasticity" of Cheng p.229

    Args:
        nu (float): poroelastic stress coefficient = alpha (1 - 2*poisson)/2/(1-poisson)
        G (float): shear modulus
        S (float): constant stress uniaxial strain storage coefficient (p. 79)

    Returns:
        pressure (float): Pressure within the specimen during the undrained response.
    """
    return -load * nu / G / S


def p_terzaghi(y, t, H, c_v, p_0, num_trunc):
    """
    Solution for the displacement of the initial undrained response of a Terzaghi consolidation problem based on the book "Poroelasticity" of Cheng p.229

    Args:
        y (float): vertical coordinate of the point
        t (float): real time
        H (float): height of the specimen
        c_v (float): Consolidation coefficient
        p_0 (float): initial pressure through the whole domain
        num_trunc (int): number of terms to be considered in the infinite summation

    Returns:
        pressure (float): Pressure at the point with coordinate yat a time t.
    """

    n = num_trunc
    res = np.zeros((len(y), len(t)))
    for k in range(1, n, 2):
        res_1 = np.sin(np.pi * k * ((H - y) / H) / 2.0)
        res_2 = np.exp(-(c_v * t / (4 * H**2)) * ((np.pi * k) ** 2.0))
        res += (4.0 / (np.pi * k)) * np.outer(res_1, res_2)
    res = res * p_0
    return res


def p_terzaghi_haagenson(y, t, H, c_v, p_0, num_trunc):
    """
    Alternative solution of the Terzaghi's consolidation
    based on the paper of Haagenson et al. 2020

    Args:
        y (float): vertical coordinate of the point
        t (float): real time
        H (float): height of the specimen
        c_v (float): Consolidation coefficient
        p_0 (float): initial pressure through the whole domain
        num_trunc (int): number of terms to be considered in the infinite summation

    Returns:
        pressure (float): Pressure at the point with coordinate y at a time t.
    """

    n = num_trunc
    res = np.zeros((len(y), len(t)))
    for k in range(1, n):
        res_1 = 1 / (2 * k - 1) * np.sin(np.pi * ((H - y) / H) / 2.0 * (2 * k - 1))
        res_2 = np.exp(-(((2 * k - 1) * np.pi / 2 / H) ** 2) * c_v * t)
        res += np.outer(res_1, res_2)
    res = res * p_0 * 4.0 / np.pi
    return res


def p_0_cryer(alpha, K, S_eps, load):
    """
    Solution for the pressure of the initial undrained response
    of a Cryer consolidation problem based on the paper of
    Haagenson et al. 2020.

    Args:
        alpha (float): biot's coefficient
        K (float): bulk modulus
        S_eps (float): drained storage coefficient

    Returns:
        pressure (float): Pressure at the center of the sphere
        during the undrained response.
    """
    return alpha / (alpha**2 + K * S_eps) * load


def p_cryer(alpha, K, S_eps, G, m_v, p_0, times, c_v, R, num_roots):
    """
    Solution for the pressure in the drained part
    of the Cryer's consolidation problem
    based on the paper of Haagenson et al. 2020

    Args:
        alpha (float): biot's coefficient
        K (float): bulk modulus
        S_eps (float): drained storage coefficient
        m_v (float): confined compressibility of the porous medium
        G (float): shear modulus
        times (float): vector of output times
        c_v (float): Consolidation coefficient
        p_0 (float): initial (undrained) pressure
        num_trunc (int): number of terms to be
        considered in the infinite summation

    Returns:
        pressure (float): Pressure at the center of the sphere
        during the drained stage at the time times.
    """
    eta = (alpha**2 + K * S_eps) / (2 * G * m_v * alpha**2)

    def func(x):
        return (1 - eta * x * x / 2) * np.tan(x) - x

    # initial guess are the roots of tan(x) = 0
    guess = np.arange(0, num_roots * np.pi, np.pi)
    roots = fsolve(func, guess)

    sin_vec = (np.sin(roots) - roots) / (
        eta * roots * np.cos(roots) / 2 + (eta - 1) * np.sin(roots)
    )
    sin_vec[0] = 0
    exp_mat = np.exp(np.outer(times, -(roots**2) * c_v / R**2))
    under_sum = np.multiply(exp_mat, sin_vec)
    p = eta * p_0 * np.sum(under_sum, axis=1)

    return p


def inclined_fault_solution(eta, b, alpha, load, E, nu, phi):
    """
    Analytical solution to the inclined frictional fault
    in an infinite body uniaxially compressed
    from "Symmetric-Galerkin BEM simulation of fracture
    with frictional contact" by Phan et al, 2003

    Args:
        eta (float): distance from the crack center along the crack [m]
        b (float): half of the crack length [m]
        alpha (float): angle of the crack inclination with respect
            to the loading axis
        load (float): value of uniaxial loading [Pa]
        E (float): Young's modulus
        nu (float): Poisson ratio
        phi (float): angle of internal friction

    Returns:
        delta_u_s (float): sliding at the specified point.
        t_n (float): normal traction at the point.
        t_s (float): shear traction at the point.
    """

    t_n = load * np.sin(alpha) * np.sin(alpha)
    t_s = t_n * np.tan(phi)
    delta_u_s = (
        4
        * (1 - nu**2)
        * load
        * np.sin(alpha)
        * (np.cos(alpha) - np.sin(alpha) * np.tan(phi))
        / E
        * np.sqrt(b**2 - (eta - b) ** 2)
    )

    return delta_u_s, t_n, t_s


def pressure_diffusion_2D(r, alpha, inj_rate_scaled, time):
    """
    Analytical solution of pressure diffusion in 2D
    for a case of continuous injection
    based on the fundamental solution in
    book of Cheng Poroelasticity (p.402)

    Args:
        r (float): distance from the injection center along the crack [m]
        alpha (float): hydraulic diffusivity
        inj_rate_scaled (float): [m3/sec] injection rate scaled
            by multiplying with fluid_viscosity and dividing by
            permeability

    Returns:
        pressure (float): pressure at the radius r from the centre
            of injection
    """

    prefactor = np.sqrt(4 * alpha * time)
    if isinstance(r, np.ndarray) and isinstance(time, np.ndarray):
        xi = np.outer(r, 1 / prefactor)
    else:
        xi = r / prefactor
    E1 = exp1(xi**2)
    pressure_unitless = -E1 / 4 / np.pi
    pressure = pressure_unitless * inj_rate_scaled
    return pressure


def pressure_diffusion_3D(r, alpha, inj_rate_scaled, time):
    """
    Analytical solution of pressure diffusion in 3D
    for a case of continuous injection
    based on the fundamental solution in
    book of Cheng Poroelasticity (p.402)

    Args:
        r (float): distance from the injection center along the crack [m]
        alpha (float): hydraulic diffusivity
        inj_rate_scaled (float): [m3/sec] injection rate scaled
            by multiplying with fluid_viscosity and dividing by
            permeability

    Returns:
        pressure (float): pressure at the radius r from the centre
            of injection
    """
    xi = r / np.sqrt(4 * alpha * time)
    erfc_val = erfc(xi)
    pressure_unitless = -1 / 4 / np.pi * erfc_val / r
    pressure = pressure_unitless * inj_rate_scaled
    return pressure


def delta_p_star(Qw, eta, k, wh):
    """
    Computes characteristic pressure for constant injection rate
    case from the paper of Saez et al. JMPS 2021

    Args:
        Qw (float): constant injection volume rate [L^3/T]
        eta (float): fluid dynamic viscosity
        k (float): constant and uniform fault permeability [L^2]
        wh (float):fault thickness [L]

    Returns:
        delta_p_star (float): characteristic pressure
    """

    delta_p = Qw * eta / (4 * np.pi * k * wh)
    return delta_p


def T_parameter(tau_0, f, sigma_0, delta_p_star):
    """
    Computes stress-injection parameter from
    paper of Saez et al. JMPS 2021

    Args:
        tau_0 (float): initial shear stress along the fault
        f (float): friction coefficient (tan of friction angle)
        sigma_0 (float): initial normal stress on the fault
        delta_p_star (float): characteristic pressure

    Returns:
        T (float): stress-injection parameter
    """
    T = (1 - tau_0 / f / sigma_0) / (delta_p_star / sigma_0)
    return T


def shear_stress_from_T(T, f, sigma_0, delta_p_star):
    """
    Computes shear stress to comply with T
    based on  paper of Saez et al. JMPS 2021

    Args:
        T (float): stress-injection parameter T
        f (float): friction coefficient (tan of friction angle)
        sigma_0 (float): initial normal stress on the fault
        delta_p_star (float): characteristic pressure

    Returns:
        tau_0 (float): shear stress
    """
    tau_0 = f * (sigma_0 - T * delta_p_star)
    return tau_0


def lambda_crit_stressed(T):
    """
    Computes amplification factor lambda for critically-
    stressed case (T<<1) based on  paper of Saez et al. JMPS 2021.

    Args:
        T (float): stress-injection parameter in critically stressed case (T << 1)

    Returns:
        lambda (float): ratio between rupture and diffusion lengthscales
    """
    lambda_ = 1 / np.sqrt(3 * T)
    return lambda_


def lambda_marg_pressurized(T):
    """
    Computes amplification factor lambda for marginally-
    pressurized case (T>>1) based on  paper of Saez et al. JMPS 2021.

    Args:
        T (float): stress-injection parameter in critically stressed case (T << 1)

    Returns:
        lambda (float): ratio between rupture and diffusion lengthscales
    """
    lambda_ = 1 / 2 * np.exp((2 - np.euler_gamma - T) / 2)
    return lambda_


def diffusion_front(k, S, eta, time):
    """
    Computes radius of the diffusion front along the fault at time t
    based on  paper of Saez et al. JMPS 2021.

    Args:
        eta (float): fluid dynamic viscosity
        k (float): constant and uniform fault permeability [L^2]
        S (float): storage coefficient
        time (float)

    Returns:
        L (float): radius of the diffusion front along the fault
    """
    alpha = k / S / eta  # fault hydraulic diffusivity
    L = np.sqrt(4 * alpha * time)
    return L


def slip_marg_pressurized(mu, f, delta_p, rupture_front, distances):
    """
    Computes slip along the direction of shear loading in marginally
    pressurized case based on  paper of Saez et al. JMPS 2021.

    Args:
        mu (float): shear modulus of bulk
        f (float): friction coefficient
        delta_p (float): characteristic pressure
        rupture_front (float): slip front position
        distances (float): distances from the injection point

    Returns:
        delta (float): slip at the specified point
    """
    r_hat = distances / rupture_front
    delta = (
        f
        * delta_p
        * rupture_front
        / mu
        * 8
        / np.pi
        * (np.sqrt(1 - r_hat**2) - np.abs(r_hat) * np.arccos(np.abs(r_hat)))
    )
    delta = np.nan_to_num(delta)
    return delta


def slip_crit_stressed(mu, f, delta_p, diffusion_front, T, rupture_front, distances):
    """
    Computes slip along the direction of shear loading in critically
    stressed case based on paper of Saez et al. JMPS 2021.

    Args:
        mu (float): shear modulus of bulk
        f (float): friction coefficient
        delta_p (float): characteristic pressure
        rupture_front (float): slip front radius
        diffusion_front (float): diffusion front radius
        T (float): stress-injection parameter
        distances (float): distances from the injection point

    Returns:
        delta (float): slip at the specified point
    """
    r_hat = distances / rupture_front
    delta = (
        f
        * delta_p
        * diffusion_front
        / mu
        * 2
        * np.sqrt(2 * T)
        / np.pi
        * (np.arccos(np.abs(r_hat)) / np.abs(r_hat) - np.sqrt(1 - r_hat**2))
    )
    return delta


def complete_slip_profile_cs(
    G, f, delta_p, diffusion_front, T, lambda_analytical, distances
):
    """
    Computes complete analytical solution for fluid driven circular
    frictional ruptures in critically stressed regime based on paper
    of paper of Viesca 2024 "Asymptotic solutions for self-similar
    fault slip induced by fluid injection at constant rate"

    Args:
        G (float): shear modulus of bulk
        f (float): friction coefficient
        delta_p (float): characteristic pressure
        rupture_front (float): slip front radius
        diffusion_front (float): diffusion front radius
        T (float): stress-injection parameter
        alpha (float): hydraulic diffusivity
        distances (float): distances from the injection point
        lambda_analytical (float): analytical solution for lambda -
            function of stress injection parameter T

    Returns:
        delta (float): slip at the specified point
    """
    lam = lambda_analytical(T)
    rupture_front = lam * diffusion_front
    rho = distances / rupture_front
    arg = lam * rho

    first_term = (
        2
        * np.sqrt(np.pi)
        * np.exp(-0.5 * arg**2)
        * (
            (1 + arg**2) * scipy.special.i0(0.5 * arg**2)
            + arg**2 * scipy.special.i1(0.5 * arg**2)
        )
        - 4 * arg
    )

    sec_term = (2 / (lam * np.pi)) * (
        np.arccos(rho) / rho - np.sqrt(1 - rho**2)
    ) - 1 / arg

    third_term = (0.25 / (lam**3 * np.pi)) * (
        np.arccos(rho) / rho**3 - np.sqrt(1 - rho**2) * (2 - 1 / rho**2)
    ) - 1 / ((2 * arg) ** 3)
    fourth_term = (3 / (16 * lam**5 * np.pi)) * (
        np.arccos(rho) / rho**5
        - np.sqrt(1 - rho**2) * (8 / 3 - 2 / (3 * rho**2) - 1 / rho**4)
    ) - 3 / ((2 * arg) ** 5)

    delta = (
        delta_p
        * f
        * diffusion_front
        / G
        * (first_term + sec_term + third_term + fourth_term)
    )

    return delta


def complete_slip_profile_mp(
    G, f, dp_star, T, diffusion_front, lambda_analytical, distances
):
    """
    Computes complete analytical solution for fluid driven circular
    frictional ruptures in marginally pressurized regime based on paper
    of paper of Viesca 2024

    Args:
        G (float): shear modulus of bulk
        f (float): friction coefficient
        delta_p (float): characteristic pressure
        diffusion_front (float): diffusion front radius
        T (float): stress-injection parameter
        distances (float): distances from the injection point
        lambda_analytical (float): analytical solution for lambda -
            function of stress injection parameter T

    Returns:
        delta (float): slip at the specified point
    """

    lam = lambda_analytical(T)
    rupture_front = lam * diffusion_front
    rho = distances / rupture_front

    first_term = (8 / np.pi) * (np.sqrt(1 - rho**2) - rho * np.arccos(rho))

    sec_term = ((lam**2 * 16) / (9 * np.pi)) * (1 - rho**2) ** (3 / 2)

    third_term = (
        lam**4 * (32 / (225 * np.pi)) * (1 - rho**2) ** (3 / 2) * (3 + 2 * rho**2)
    )

    delta = (
        dp_star * f * lam * diffusion_front / G * (first_term + sec_term + third_term)
    )
    delta = np.nan_to_num(delta)

    return delta


def lambda_analytical(T):
    """
    Computes lambda based on the value of T
    Eq 21 of paper of Viesca 2024

    Args:
        T (float): stress-injection parameter

    Returns:
        lambda (float): amplification factor
    """

    def lam2():
        return 2

    eq = (
        -mpmath.euler
        + (2 / 3) * lam2 * mpmath.hyp2f2(1, 1, 2, 2.5, -lam2)
        - mpmath.log(4 * lam2)
        - T
    )
    if T >= 1:
        lambda_approx = lambda_marg_pressurized(T)
    else:
        lambda_approx = lambda_crit_stressed(T)
    return float(mpmath.sqrt(mpmath.findroot(eq, lambda_approx**2).real))


############################################################################
############ point force acting on a stressed fault ###############################
###### Brice's derivation (via email) #############################


def slip_front_point_force(P, f, sigma_0, tau_0):
    # Input:
    # P - point load
    # f - friction coefficient
    # sigma_0 - normal compression
    # tau_0 - initial shear load
    R = 1 / np.sqrt(2 * np.pi) * np.sqrt(f * P / (f * sigma_0 - tau_0))
    return R


def slip_profile_point_force(rupture_front, distances, tau_0, f, sigma_0, mu, P):
    # Input:
    # P - point load
    # mu - shear modulus of bulk
    # f - friction coefficient
    # rupture_front - position at specific time moment
    # distances from the injection point
    # tau_0 - initial shear load
    # sigma_0 - initial compression stress

    delta = 4 * (tau_0 - f * sigma_0) / np.pi / mu * (
        rupture_front**2 - distances**2
    ) + 2 * f / np.pi**2 / mu * P / distances * np.arccos(
        np.abs(distances / rupture_front)
    )
    return delta


############################################################################
######### circular pressure patch acting on a stressed fault ###############
############### Brice's derivation (via email) #############################


def slip_front_patch_pressure(p, rw, f, sigma_0, tau_0):
    # Input:
    # p - pressure
    # rw - radius of a patch where pressure is applied
    # f - friction coefficient
    # sigma_0 - normal compression
    # tau_0 - initial shear load
    R = rw * f * p / np.sqrt((f * sigma_0 - tau_0) * (tau_0 + 2 * f * p - f * sigma_0))
    return R


def slip_profile_patch_pressure(p, rw, f, sigma_0, tau_0, rupture_front, distances, mu):
    # Input:
    # p - pressure
    # rw - radius of a patch where pressure is applied
    # mu - shear modulus of bulk
    # f - friction coefficient
    # rupture_front - position at specific time moment
    # distances from the injection point
    # tau_0 - initial shear load
    # sigma_0 - initial compression stress
    rhos = abs(distances / rupture_front)
    m = rw / rupture_front
    delta_rims = np.zeros_like(rhos)
    for point in range(len(rhos)):
        rho = rhos[point]
        if rho < m:
            ratio = np.sqrt((1 - m**2) / (1 - rho**2))
            delta_rims[point] = (
                np.sqrt(1 - rho**2)
                - ratio
                + m * ellipeinc(np.arcsin(ratio), (rho / m) ** 2)
            )
        elif rho <= 1:
            ratio = np.sqrt((1 - rho**2) / (1 - m**2))
            delta_rims[point] = (
                np.sqrt(1 - rho**2)
                - ratio
                + rho * ellipeinc(np.arcsin(ratio), (m / rho) ** 2)
                + (m**2 - rho**2) / rho * ellipkinc(np.arcsin(ratio), (m / rho) ** 2)
            )
        else:
            delta_rims[point] = 0

    delta = (
        4
        * (tau_0 - f * sigma_0)
        / np.pi
        / mu
        * np.sqrt(np.maximum(rupture_front**2 - distances**2, 0))
        + 4 * f * p * rupture_front * delta_rims / np.pi / mu
    )
    return delta


############################################################################
############ tensile point force acting on penny-shaped crack ##############
############################### Tada p. 343 ################################


def opening_profile_point_load(distance, P, nu, E, crack_length):
    # Input:
    # P - point load
    # distance - distance from the crack center
    # nu - Poisson ratio
    # E - Young modulus
    # crack_length

    opening = (
        4
        * (1 - nu**2)
        / np.pi**2
        / E
        * P
        / distance
        * np.arccos(distance / crack_length)
    )
    return opening
