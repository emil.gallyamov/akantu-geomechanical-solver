### Benchmarking the geomechanical simulator with Cryer's sphere problem.

*cryer_sphere.ipynb* is the Jupyter Notebook which explains the simulation steps and compares the simulated results to the analytical solution.

*cryer_sphere.py* does the same simulation as in the Jupyter Notebook, but it can be run in parallel. A parallel job can be started as

        mpiexec -np N python cryer_sphere.py --output_folder folder_name --mesh_file sphere_coarse.msh --time_steps X --material_file material.dat

where *N* is the number of processes and *X* is the number of time steps. The code will create the output folder if it doesn't yet exist. It will store paraview output files, pressure at the center of the sphere and the time value for each time in this folder.

*plot_pressure.py* plots pressure at the sphere centre from the *output_folder/central_pressure.npy* file versus time from the *output_folder/times.npy* file.

*geometry.py* creates FEM mesh of one eights of a sphere with its centre at origin coordinate. User can modify the dimensions of the mesh and its resolution.
