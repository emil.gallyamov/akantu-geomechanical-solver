#!/usr/bin/env python3
""" plot_pressure.py: Plots the pressure at the center of the sphere vs analytical solution"""

__author__ = "Emil Gallyamov"
__credits__ = [
    "Emil Gallyamov <emil.gallyamov@epfl.ch>",
]
__copyright__ = (
    "Copyright (©) 2016-2021 EPFL (Ecole Polytechnique Fédérale"
    " de Lausanne) Laboratory (LSMS - Laboratoire de Simulation"
    " en Mécanique des Solides)"
)
__license__ = "LGPLv3"

import matplotlib.pyplot as plt
import numpy as np
import sys
import os

sys.path.append(os.path.join(os.path.join(sys.path[0], ".."), "analytical_solutions"))

import analytical_solutions as an_sol

central_pressure = np.load("./output/central_pressure.npy")
times = np.load("./output/times.npy")

# geometric parameters
ball_r = 1  # [m] sphere radius
h = 0.1  # [m] element size

# material parameters
phi_0 = 0.05  # [-] porosity
K = 10  # [GPa] bulk modulus
G = 6  # [GPa] shear modulus consistent with K and Poisson
poisson = (3 * K - 2 * G) / 2 / (3 * K + G)  # [-] Poisson ratio
E = 9 * K * G / (3 * K + G)  # [GPa] Young modulus
alpha = 0.9  # [-] Biot's coefficient
one_over_M = 3.05e-2  # [1/GPa] Biot's modulus
k = 1e-18  # [m2] permeability
mu = 8.9e-13  # [GPa s] viscosity
load = 5  # [GPa] radial compression
S = one_over_M + 3 * alpha**2 / (3 * K + 4 * G)  # storage coefficient
c_v = k / mu / S  # generalized consolidation coefficient (Cheng's book, p.198)
final_time = ball_r * ball_r / c_v  # [s] final time
nb_timesteps = 50
timestep = final_time / nb_timesteps
times = np.linspace(0, final_time, nb_timesteps + 1)
# [1/GPa] confined compressibility of the porous medium (see Haagenson et al. 2020)
m_v = 1 / (K + 4 / 3 * G)

beta_s = 1.0e-2  # [1/GPa] compressibility of solid particles
beta_m = 1.0e-1  # [1/GPa] compressibility of the porous media
beta_f = 4.4e-1  # [1/GPa] compressibility of the fluid
# [1/GPa] drained storage coefficient (see Haagenson et al. 2020)
S_eps = (alpha - phi_0) * beta_s + phi_0 * beta_f


times_star = c_v * times / ball_r / ball_r
p_0_an = an_sol.p_0_cryer(alpha, K, S_eps, load)
p_analytic = np.zeros(times.size)
p_analytic[0] = p_0_an
p_analytic[1:] = an_sol.p_cryer(
    alpha, K, S_eps, G, m_v, p_0_an, times[1:], c_v, ball_r, 100
)


fig2, (ax6, ax7) = plt.subplots(2, 1, layout="constrained")
ax6.plot(
    times_star, central_pressure / p_0_an, linestyle=" ", marker="o", label="Numerics"
)
ax6.plot(times_star, p_analytic / p_0_an, color="k", label="Analytics")
ax6.set_ylabel(r"$p^*_c$")
ax6.legend(loc="best")
ax6.xaxis.set_tick_params(labelbottom=False)
ax6.grid()

ax7.scatter(times_star, abs(p_analytic - central_pressure) / np.max(p_analytic) * 100)
ax7.set_xlabel(r"$t^*$")
ax7.set_ylabel("Error, $e$ [%]")
ax7.grid()
plt.show()
