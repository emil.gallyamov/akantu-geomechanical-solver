#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Simulation of the Mandel-Cryer effect on 1/8 of a sphere.

This script runs the Cryer sphere simulation and saves results in output files. Data to be saved include vtk files with the solid mechanics and fluid fields, and npy files with the pressure at the sphere centre and the the output times.

Example:
    The code has some optional arguments to be provided. The list of the arguments can be consulted by running the following lines::
        python cryer_sphere.py --help

    The code can be run on a single processor and in parallel. When running in parallel, following lines should be run::
        mpiexec -np N python cryer_sphere.py --material_file material.dat
    In the above code, N is the number of processors to be employed.

    Note:
        When running in parallel, providing a material file is mandatory.
"""

__author__ = "Emil Gallyamov"
__credits__ = [
    "Emil Gallyamov <emil.gallyamov@epfl.ch>",
]
__copyright__ = (
    "Copyright (©) 2016-2021 EPFL (Ecole Polytechnique Fédérale"
    " de Lausanne) Laboratory (LSMS - Laboratoire de Simulation"
    " en Mécanique des Solides)"
)
__license__ = "LGPLv3"

try:
    from mpi4py import MPI

    comm = MPI.COMM_WORLD
    prank = comm.Get_rank()
    psize = comm.Get_size()
except ImportError:
    prank = 0

import akantu as aka
import numpy as np
import argparse
import os
import sys

from akantu_geomechanical_solver import AkantuGeomechanicalSolver
from akantu_geomechanical_solver import IterativeOptions
sys.path.append(os.path.join(sys.path[0], '..', 'analytical_solutions'))
import analytical_solutions as an_sol


class FixedPressure(aka.DirichletFunctor):
    def __init__(self, value):
        super().__init__()
        self.value = value

    def __call__(self, node, flags, primal, coord):
        # sets the blocked dofs vector to true in the desired axis
        flags[0] = True
        primal[0] = self.value


# ------------------------------------------------------------ #
# main
# ------------------------------------------------------------ #


def main():

    # parsing the input parameters
    parser = argparse.ArgumentParser(
        description="Optional arguments to be provided for the simulation of Cryer sphere"
    )
    parser.add_argument(
        "-o",
        "--output_folder",
        type=str,
        help="precise the output folder",
        default="output",
    )
    parser.add_argument(
        "-m",
        "--mesh_file",
        type=str,
        help="provide the mesh file",
        default="sphere_coarse.msh",
    )
    parser.add_argument(
        "-s", "--time_steps", type=int, help="number of time steps", default=50
    )
    parser.add_argument(
        "-f",
        "--material_file",
        type=str,
        help="provide material file when running in parallel",
        default="material.dat",
    )

    args = parser.parse_args()
    output_folder = args.output_folder
    output_path = os.path.join("./", output_folder)
    os.makedirs(output_path, exist_ok=True)
    para_dir = os.path.join(output_path, "paraview")
    os.makedirs(para_dir, exist_ok=True)
    mesh_file = args.mesh_file
    nb_timesteps = args.time_steps
    if psize > 1:
        material_file_name = args.material_file

    # geometric parameters
    ball_r = 1  # [m] sphere radius

    # material parameters
    phi_0 = 0.05  # [-] porosity
    K = 10  # [GPa] bulk modulus
    G = 6  # [GPa] shear modulus consistent with K and Poisson
    poisson = (3 * K - 2 * G) / 2 / (3 * K + G)  # [-] Poisson ratio
    E = 9 * K * G / (3 * K + G)  # [GPa] Young modulus
    alpha = 0.9  # [-] Biot's coefficient
    one_over_M = 3.05e-2  # [1/GPa] Biot's modulus
    k = 1e-18  # [m2] permeability
    mu = 8.9e-13  # [GPa s] viscosity
    load = 5  # [GPa] radial compression
    S = one_over_M + 3 * alpha**2 / (3 * K + 4 * G)  # storage coefficient
    # generalized consolidation coefficient (Cheng's book, p.198)
    c_v = k / mu / S
    final_time = ball_r * ball_r / c_v  # [s] final time
    timestep = final_time / nb_timesteps
    times = np.linspace(0, final_time, nb_timesteps + 1)
    # [1/GPa] confined compressibility of the porous medium (see Haagenson et al. 2020)

    beta_s = 1.0e-2  # [1/GPa] compressibility of solid particles
    beta_f = 4.4e-1  # [1/GPa] compressibility of the fluid
    # [1/GPa] drained storage coefficient (see Haagenson et al. 2020)
    S_eps = (alpha - phi_0) * beta_s + phi_0 * beta_f

    if psize == 1:
        material_file = f"""
        material elastic [
            name = rock
            E   = {E}   # young's modulus [GPa]
            nu  = {poisson}      # poisson's ratio
        ]

        model heat_transfer_model [
                density = 1  #kg/m3
                conductivity = [[{k/mu}, 0, 0], \
                                [0,  {k/mu}, 0], \
                                [0,  0, {k/mu}]] # m2/(GPa s)
                capacity = {one_over_M}        # 1/GPa
        ]
        """
        # writing the material file
        open("material.dat", "w").write(material_file)
        material_file_name = "material.dat"

    # reading the material file
    aka.parseInput(material_file_name)

    # reading the mesh
    dim = 3
    mesh = aka.Mesh(dim)
    if prank == 0:
        mesh.read(mesh_file)

    mesh.distribute()

    rock_model = aka.SolidMechanicsModel(mesh)
    flow_model = aka.HeatTransferModel(mesh)

    rock_model.initFull(_analysis_method=aka._static)
    flow_model.initFull(_analysis_method=aka._implicit_dynamic)
    flow_model.setTimeStep(timestep)

    # set the displacement/Dirichlet boundary conditions
    rock_model.applyBC(aka.FixedValue(0.0, aka._x), "left")
    rock_model.applyBC(aka.FixedValue(0.0, aka._y), "back")
    rock_model.applyBC(aka.FixedValue(0.0, aka._z), "bottom")

    # set the force/Neumann boundary conditions
    surface_stress = np.eye(dim)
    surface_stress *= load
    rock_model.applyBC(aka.FromStress(surface_stress), "surface")

    # dumping
    rock_model.setBaseNameToDumper("solid_mechanics_model", "solid")
    rock_model.setDirectoryToDumper("solid_mechanics_model", para_dir)
    rock_model.addDumpFieldVectorToDumper(
        "solid_mechanics_model", "displacement")
    rock_model.addDumpFieldVectorToDumper(
        "solid_mechanics_model", "internal_force")
    rock_model.addDumpFieldVectorToDumper(
        "solid_mechanics_model", "external_force")
    rock_model.addDumpFieldToDumper("solid_mechanics_model", "strain")
    rock_model.addDumpFieldToDumper("solid_mechanics_model", "stress")
    rock_model.addDumpFieldToDumper("solid_mechanics_model", "blocked_dofs")
    rock_model.addDumpFieldToDumper("solid_mechanics_model", "partitions")

    flow_model.setBaseNameToDumper("heat_transfer", "fluid")
    flow_model.setDirectoryToDumper("heat_transfer", para_dir)
    flow_model.addDumpFieldToDumper("heat_transfer", "temperature")
    flow_model.addDumpFieldToDumper("heat_transfer", "temperature_rate")
    flow_model.addDumpFieldToDumper("heat_transfer", "internal_heat_rate")
    flow_model.addDumpFieldToDumper("heat_transfer", "external_heat_rate")
    rock_model.dump("solid_mechanics_model")
    flow_model.dump("heat_transfer")

    # configure the linear algebra solver
    flow_solver = flow_model.getNonLinearSolver()
    flow_solver.set("max_iterations", 10)
    # pressure has different order of magnitude than u
    flow_solver.set("threshold", 1e-7)
    flow_solver.set("convergence_type", aka.SolveConvergenceCriteria.residual)
    flow_model.setTimeStep(timestep)

    rock_solver = rock_model.getNonLinearSolver()
    rock_solver.set("max_iterations", 20)
    rock_solver.set("threshold", 1e-6)
    rock_solver.set("convergence_type", aka.SolveConvergenceCriteria.residual)

    # setup Geomechanic solver
    it_options = IterativeOptions(
        pcg_maxiterations=50,
        newton_maxiterations=100,
        f_rtol=1e-9,
        f_rock_tol=1e-5,
        f_flow_tol=1e-8,
    )
    solver = AkantuGeomechanicalSolver(
        rock_model, flow_model, alpha, dump_iterations=False, update_stiffness=False
    )

    # prepare the initial guess arrays for the PCG solver
    nb_nodes = mesh.getNbNodes()
    delta_u_0 = np.zeros([nb_nodes, dim])
    delta_p_0 = np.zeros([nb_nodes, 1])

    # identify the central node for outputting the pressure
    central_node = 0
    central_node_found = False
    nodes = mesh.getNodes()
    node_flags = mesh.getNodesFlags()
    for node_nb in range(mesh.getNbNodes()):
        if np.allclose(nodes[node_nb], [0, 0, 0]) and node_flags[node_nb] == np.uint8(
            aka._normal
        ):
            central_node = node_nb
            central_node_found = True

    central_pressure = np.zeros(nb_timesteps + 1)

    # fix analytic pressure for the undrained step
    p_0_an = an_sol.p_0_cryer(alpha, K, S_eps, load)
    pressure = flow_model.getTemperature()
    blocked_dofs_p = flow_model.getBlockedDOFs()
    pressure.fill(p_0_an)
    blocked_dofs_p.fill(True)

    # solve undrained response first
    cvged = solver.solve_step(delta_u_0, delta_p_0, it_options)
    if not cvged:
        rock_model.assembleInternalForces()
        flow_model.assembleInternalHeatRate()
        rock_model.dump("solid_mechanics_model")
        flow_model.dump("heat_transfer")
        raise Exception("Time step solver did not converge")

    # dumping solutions for plotting
    rock_model.assembleInternalForces()
    flow_model.assembleInternalHeatRate()
    rock_model.dump("solid_mechanics_model")
    flow_model.dump("heat_transfer")
    if central_node_found:
        central_pressure[0] = pressure[central_node]

    # applying fixed pressure at the top
    blocked_dofs_p.fill(False)
    flow_model.applyBC(FixedPressure(0.0), "surface")

    for i in range(nb_timesteps):
        if prank == 0:
            print(f"\nSolving time step {i}", flush=True)

        cvged = solver.solve_step(delta_u_0, delta_p_0, it_options)
        if not cvged:
            rock_model.assembleInternalForces()
            flow_model.assembleInternalHeatRate()
            rock_model.dump("solid_mechanics_model")
            flow_model.dump("heat_transfer")
            raise Exception("Time step solver did not converge")

        # dumping solutions for plotting
        rock_model.assembleInternalForces()
        flow_model.assembleInternalHeatRate()
        rock_model.dump("solid_mechanics_model")
        flow_model.dump("heat_transfer")
        if central_node_found:
            central_pressure[i + 1] = pressure[central_node]

    # dumping results
    if central_node_found:
        np.save(os.path.join(output_path, "central_pressure.npy"), central_pressure)
        np.save(os.path.join(output_path, "times.npy"), times)


# -----------------------------------------------------------------------------
if __name__ == "__main__":
    main()
