#!/usr/bin/env python3
""" geometry.py: Builds mesh for 1/8  of a sphere"""

__author__ = "Emil Gallyamov"
__credits__ = [
    "Emil Gallyamov <emil.gallyamov@epfl.ch>",
]
__copyright__ = (
    "Copyright (©) 2016-2021 EPFL (Ecole Polytechnique Fédérale"
    " de Lausanne) Laboratory (LSMS - Laboratoire de Simulation"
    " en Mécanique des Solides)"
)
__license__ = "LGPLv3"


import gmsh

ball_r = 1  # sphere radius
h = 0.3  # element size


gmsh.initialize()
gmsh.model.add("sphere")

p1 = gmsh.model.geo.addPoint(0, 0, 0, h)
p2 = gmsh.model.geo.addPoint(ball_r, 0, 0, h)
p3 = gmsh.model.geo.addPoint(0, ball_r, 0, h)
p4 = gmsh.model.geo.addPoint(0, 0, ball_r, h)
l1 = gmsh.model.geo.addLine(1, 2)
l2 = gmsh.model.geo.addLine(1, 3)
l3 = gmsh.model.geo.addLine(1, 4)
c1 = gmsh.model.geo.addCircleArc(p2, p1, p3)
c2 = gmsh.model.geo.addCircleArc(p2, p1, p4)
c3 = gmsh.model.geo.addCircleArc(p3, p1, p4)
cl1 = gmsh.model.geo.addCurveLoop([l2, -c1, -l1])
cl2 = gmsh.model.geo.addCurveLoop([l3, -c2, -l1])
cl3 = gmsh.model.geo.addCurveLoop([l2, c3, -l3])
cl4 = gmsh.model.geo.addCurveLoop([c2, -c3, -c1])
ps1 = gmsh.model.geo.addPlaneSurface([cl1])
ps2 = gmsh.model.geo.addPlaneSurface([cl2])
ps3 = gmsh.model.geo.addPlaneSurface([cl3])
ps4 = gmsh.model.geo.addSurfaceFilling([cl4])

sl = gmsh.model.geo.addSurfaceLoop([ps1, ps2, ps3, ps4])
V = gmsh.model.geo.addVolume([sl])

gmsh.model.addPhysicalGroup(2, [ps1], name="bottom")
gmsh.model.addPhysicalGroup(2, [ps2], name="back")
gmsh.model.addPhysicalGroup(2, [ps3], name="left")
gmsh.model.addPhysicalGroup(2, [ps4], name="surface")
gmsh.model.addPhysicalGroup(3, [V], name="sphere")

gmsh.model.geo.synchronize()
gmsh.option.setNumber("Mesh.Smoothing", 100)
gmsh.model.mesh.generate(3)
gmsh.write("sphere.msh")
gmsh.finalize()
