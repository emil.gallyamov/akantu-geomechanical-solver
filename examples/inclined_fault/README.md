### Benchmarking the mechanical part of the geomechanical simulator with inclined frictional fault problem.

*inclined_fault.ipynb* is the Jupyter Notebook which explains the simulation steps and compares the simulated results to the analytical solution.
