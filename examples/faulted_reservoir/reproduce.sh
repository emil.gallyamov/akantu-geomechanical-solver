python3 geometry_reservoir_full.py
mpiexec -np 72 python3 reservoir_injection.py --output_folder=output -m=faulted_reservoir_full_dense.msh -f=material.dat --fixed_stiffness -s=300 -r=0
