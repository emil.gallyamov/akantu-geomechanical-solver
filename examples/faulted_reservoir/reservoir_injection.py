#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Simulation of the fluid injection into an aquifer crossed by a fault.

This script runs the injection into an aquifer and saves results in the output folder. Data to be saved include vtk files with the solid mechanics and fluid fields, and npy files with the pressure, slip, opening, stress, etc.
"""

__author__ = "Emil Gallyamov"
__credits__ = [
    "Emil Gallyamov <emil.gallyamov@epfl.ch>",
]
__copyright__ = (
    "Copyright (©) 2016-2021 EPFL (Ecole Polytechnique Fédérale"
    " de Lausanne) Laboratory (LSMS - Laboratoire de Simulation"
    " en Mécanique des Solides)"
)
__license__ = "LGPLv3"

try:
    from mpi4py import MPI

    comm = MPI.COMM_WORLD
    prank = comm.Get_rank()
    psize = comm.Get_size()
except ImportError:
    prank = 0

import akantu as aka
import numpy as np
import argparse
import os
import math
import sys

from akantu_geomechanical_solver import AkantuGeomechanicalSolver
from akantu_geomechanical_solver import IterativeOptions
sys.path.append(os.path.join(sys.path[0], '..', 'analytical_solutions'))


class FixedPressure(aka.DirichletFunctor):
    # creating a functor to fix pressures along surfaces
    def __init__(self, value):
        super().__init__()
        self.value = value

    def __call__(self, node, flags, primal, coord):
        # sets the blocked dofs vector to true in the desired axis
        flags[0] = True
        primal[0] = self.value


class lateralCompression(aka.NeumannFunctor):
    # this functor applies lateral compression on the model increasing with depth. It assumes that the bottom of the model is at 0 z coordinate and the top of the model is at depth "starting_depth". It applies load always normally to the surface.
    def __init__(self, K_0, starting_depth, model_height, density, gravity=9.81):
        super().__init__()
        self.K_0 = K_0
        self.starting_depth = starting_depth
        self.model_height = model_height
        self.density = density
        self.gravity = gravity

    def __call__(self, quad_point, dual, coord, normals):
        quad_depth = self.starting_depth + self.model_height - coord[-1]
        horiz_force = self.K_0 * quad_depth * self.density * self.gravity
        np.copyto(dual, horiz_force * normals)


class lateralCompressionStress(aka.NeumannFunctor):
    # this is the similar functor as before, but it can apply load under certain angle to a surface. It rotates the initial stress matrix by a user-provided clockwise angle and projects it on the surface.
    def __init__(
        self,
        K_0x,
        K_0y,
        clockwise_rot_angle,
        starting_depth,
        model_height,
        density,
        gravity=9.81,
    ):
        super().__init__()
        dim = 3
        unrotated_unscaled_stress = np.zeros([dim, dim])
        unrotated_unscaled_stress[0, 0] = K_0x * density * gravity
        unrotated_unscaled_stress[1, 1] = K_0y * density * gravity
        rot_angle_rad = clockwise_rot_angle / 180 * np.pi
        rot_matrix = np.array(
            [
                [np.cos(rot_angle_rad), -np.sin(rot_angle_rad), 0],
                [np.sin(rot_angle_rad), np.cos(rot_angle_rad), 0],
                [0, 0, 1],
            ]
        )
        self.rotated_unscaled_stress = np.dot(
            rot_matrix, unrotated_unscaled_stress)
        self.rotated_unscaled_stress = np.dot(
            self.rotated_unscaled_stress, rot_matrix.T
        )
        self.starting_depth = starting_depth
        self.model_height = model_height

    def __call__(self, quad_point, dual, coord, normals):
        quad_depth = self.starting_depth + self.model_height - coord[-1]
        rotated_scaled_stress = self.rotated_unscaled_stress * quad_depth
        np.copyto(dual, np.dot(rotated_scaled_stress, normals))


class lateralCompressionNormal(aka.NeumannFunctor):
    # This functor takes normal to the surface as an argument
    def __init__(
        self,
        K_0x,
        K_0y,
        clockwise_rot_angle,
        starting_depth,
        model_height,
        density,
        normal,
        gravity=9.81,
    ):
        super().__init__()
        dim = 3
        unrotated_unscaled_stress = np.zeros([dim, dim])
        unrotated_unscaled_stress[0, 0] = K_0x * density * gravity
        unrotated_unscaled_stress[1, 1] = K_0y * density * gravity
        rot_angle_rad = clockwise_rot_angle / 180 * np.pi
        rot_matrix = np.array(
            [
                [np.cos(rot_angle_rad), -np.sin(rot_angle_rad), 0],
                [np.sin(rot_angle_rad), np.cos(rot_angle_rad), 0],
                [0, 0, 1],
            ]
        )
        self.rotated_unscaled_stress = np.dot(
            rot_matrix, unrotated_unscaled_stress)
        self.rotated_unscaled_stress = np.dot(
            self.rotated_unscaled_stress, rot_matrix.T
        )
        self.starting_depth = starting_depth
        self.model_height = model_height
        self.normal = normal

    def __call__(self, quad_point, dual, coord, normals):
        quad_depth = self.starting_depth + self.model_height - coord[-1]
        rotated_scaled_stress = self.rotated_unscaled_stress * quad_depth
        np.copyto(dual, np.dot(rotated_scaled_stress, self.normal))


# this function applies body force on the whole model according to its density
def applyBodyForce(model, gravity=9.81):
    spatial_dimension = model.getSpatialDimension()
    model.assembleMassLumped()
    mass = model.getMass()
    force = model.getExternalForce()
    gravity_vector = np.zeros(spatial_dimension)
    gravity_vector[spatial_dimension - 1] = -gravity

    for [mass_vec, force_vec] in zip(mass, force):
        force_vec += gravity_vector * mass_vec


class MaterialFrictionalFault(aka.MaterialCohesiveLinearFriction3D):
    # daughter class which inherits from cohesive material with linear friction
    def __init__(self, model, _id):
        super().__init__(model, _id)
        dim = self.getModel().getSpatialDimension()
        self.registerInternalReal("contact_opening_normal", 1)
        self.registerInternalReal("contact_tractions_normal", 1)
        self.registerInternalReal("friction_force_norm", 1)
        self.registerInternalReal("sliding_norm", 1)
        self.registerInternalReal("quad_coordinates", dim)

    def initMaterial(self):
        super().initMaterial()
        delta_c = self.getReal("delta_c")
        sigma_c = self.getReal("sigma_c")
        G_c = self.getReal("G_c")
        self.setBool("contact_after_breaking", True)
        self.fix_cohesive_state = False

        if delta_c != 0.0:
            self.setDefaultValueToInternalReal("delta_max", delta_c)
            self.setDefaultValueToInternalPreviousReal("delta_max", delta_c)
        else:
            self.setDefaultValueToInternalReal("delta_max", 2 * G_c / sigma_c)
            self.setDefaultValueToInternalPreviousReal(
                "delta_max", 2 * G_c / sigma_c)

    # override: fixing stiffness matrix if within PCG iterations
    def hasStiffnessMatrixChanged(self):
        if self.fix_cohesive_state:
            return False
        else:
            return True

    # override: checks if cohesive elements are in contact
    def checkPenetration(self, openings, normals, penetrations, el_type, ghost_type):
        penetrations.fill(True)

    # override: computes normals to cohesive elements
    def computeNormal(self, position, normals, type, ghost_type):
        if self.fix_cohesive_state:
            # we don't modify normals when in PCG. Normals is the cohesive internal field
            pass
        else:
            super().computeNormal(position, normals, type, ghost_type)

    # constitutive law
    def computeTraction(self, normals, el_type, ghost_type):
        if self.fix_cohesive_state:
            # the only time traction has to be computed in PCG is when solving A11 namely when updating residual AFTER the actual solution of the system therefore, we do not care about the residual after the system is solved so we skip
            pass
        else:
            super().computeTraction(normals, el_type, ghost_type)
            openings = self.getOpening(el_type, ghost_type)
            contact_openings = self.getInternalReal("contact_opening")(
                el_type, ghost_type
            )
            contact_tractions = self.getInternalReal("contact_tractions")(
                el_type, ghost_type
            )
            friction_forces = self.getInternalReal("friction_force")(
                el_type, ghost_type
            )
            openings = self.getInternalReal("opening")(el_type, ghost_type)
            contact_opening_normal = self.getInternalReal("contact_opening_normal")(
                el_type, ghost_type
            )
            contact_tractions_normal = self.getInternalReal("contact_tractions_normal")(
                el_type, ghost_type
            )
            friction_forces_norm = self.getInternalReal("friction_force_norm")(
                el_type, ghost_type
            )
            sliding_norm = self.getInternalReal(
                "sliding_norm")(el_type, ghost_type)
            for quad in range(normals.shape[0]):
                # compute normal and tangential components of opening
                normal = normals[quad].ravel()
                contact_opening = contact_openings[quad].copy()
                contact_opening_normal[quad] = abs(contact_opening.dot(normal))
                contact_traction = contact_tractions[quad].copy()
                contact_tractions_normal[quad] = abs(
                    contact_traction.dot(normal))
                friction_force = friction_forces[quad].copy()
                friction_forces_norm[quad] = np.linalg.norm(friction_force)
                sliding_norm[quad] = np.linalg.norm(openings[quad])

    # state of cohesive elements is fixed in the beginning of PCG solve
    def fixCohesiveState(self):
        self.fix_cohesive_state = True

    # it is unfixed at the end of PCG solve
    def unfixCohesiveState(self):
        self.fix_cohesive_state = False

    # previous state is not saved when in PCG
    def savePreviousState(self):
        if self.fix_cohesive_state:
            pass
        else:
            super().savePreviousState()


def allocator(_dim, unused, model, _id):
    return MaterialFrictionalFault(model, _id)


# ---------------------------------------------------------------
# main
# ---------------------------------------------------------------


def main():

    # parsing the input parameters
    parser = argparse.ArgumentParser(
        description="Parameters of the parser for simulation of the faulted aquifer"
    )
    parser.add_argument(
        "-o",
        "--output_folder",
        type=str,
        help="precise the output folder",
        default="output",
    )
    parser.add_argument(
        "--dump_iterations",
        action="store_true",
        help="dump paraview at every Newton iterations",
    )
    parser.add_argument(
        "-f", "--material_file", type=str, help="material file", default="material.dat"
    )
    parser.add_argument(
        "-m",
        "--mesh_file",
        type=str,
        help="provide the mesh file",
        default="faulted_reservoir_coarse.msh",
    )
    parser.add_argument(
        "--update_stiffness",
        action="store_true",
        help="update stiffness when contact is plastifying",
    )
    parser.add_argument(
        "-s", "--time_steps", type=int, help="number of time steps", default=120
    )
    parser.add_argument(
        "-r",
        "--rotation_angle",
        type=float,
        help="clockwise stress rotation angle around z (in degrees)",
        default=0,
    )

    args = parser.parse_args()
    output_folder = args.output_folder
    output_path = os.path.join("./", output_folder)
    os.makedirs(output_path, exist_ok=True)
    para_dir = os.path.join(output_path, "paraview")
    os.makedirs(para_dir, exist_ok=True)
    mesh_file = args.mesh_file
    dump_iterations = args.dump_iterations
    update_stiffness = args.update_stiffness
    nb_timesteps = args.time_steps
    clockwise_rotation_angle = args.rotation_angle
    if psize > 1:
        material_file_name = args.material_file

    # model size
    H = 2000  # [m]
    W = 2000  # [m]
    h_aq = 100  # [m]
    top_depth = 500  # [m] depth of the top surface of the model
    injection_rate = 0.04  # [m3 / s]
    gravity = 9.81  # [m/s2] or [N / kg]
    K0_x = 0.7  # coefficient of lateral pressure in x direction
    K0_y = 1.2
    timestep_days = 30  # [days]
    sec_in_day = 24 * 60 * 60  # [s]
    coh_type = aka._cohesive_3d_6

    # rock properties
    rock_density = 1700  # [kg / m3] - value after subtracting water density
    K = 13e9  # [Pa] bulk modulus
    poisson = 0.15  # [-] Poisson ratio
    E = 3 * K * (1 - 2 * poisson)  # [Pa] Young modulus
    alpha = 0.64  # [-] Biot coefficient
    aquifer_permeability = 1.0e-15  # [m2] 1 md = 1e-15 m2
    # [m2] permeability of cap rock (range fits values on https://en.wikipedia.org/wiki/Permeability_(Earth_sciences))
    cap_permeability = 1.0e-20
    phi_0 = 0.06  # [-] initial porosity
    beta_s = (1 - alpha) / K  # [1/Pa] sand particles compressibility
    beta_f = 4.6e-10  # [1/Pa] water compressibility
    storage = (alpha - phi_0) * beta_s + phi_0 * \
        beta_f  # [Pa-1] storage coefficient
    fluid_viscosity = 8.9e-4  # [Pa s]  water viscosity
    rock_diffusivity = aquifer_permeability / storage / fluid_viscosity
    timescale = (W / 2) ** 2 / 4 / rock_diffusivity
    if prank == 0:
        print(f"Timescale {timescale/sec_in_day} day(s)")

    # fault properties
    kh = 1e-15  # [m3] fault transmissibility (k * h)
    # [m] hydraulic aperture from cubic law kh = (w^3)/12
    hydraulic_opening = (kh * 12) ** (1.0 / 3)
    longitudinal_permeability = kh / hydraulic_opening  # [m2]
    fault_diffusivity = 1e-3  # [m2 /s] fracture hydraulic diffusivity

    # [1/Pa] combined effect of fluid compressibility and fault porosity
    storage_in_crack = longitudinal_permeability / \
        fault_diffusivity / fluid_viscosity
    transversal_permeability = (
        longitudinal_permeability * hydraulic_opening
    )  # [m2 / (Pa s)]

    # mechanical properties of the fault
    friction_coef = 0.6
    penetration_penalty = E * 100  # fault normal stiffness
    penalty_for_friction = E * 10  # fault shear stiffness

    # register material to the MaterialFactory
    mat_factory = aka.MaterialFactory.getInstance()
    mat_factory.registerAllocator("material_frictional_fault", allocator)

    if psize == 1:
        material_description = f"""
        model solid_mechanics_model_cohesive [

        material elastic [
                name = whatever
                rho = {rock_density}   # density
                E   = {E} # young's modulus
                nu  = {poisson}    # poisson's ratio
        ]
        material material_frictional_fault [
                name = fault
                beta = 1
                G_c = 20
                penalty = {penetration_penalty} # stiffness in compression to prevent penetration
                sigma_c = 1.0 # we do not care
                contact_after_breaking = True
                mu = {friction_coef}  # Max value of the friction coefficient
                penalty_for_friction = {penalty_for_friction}
        ]
        ]
        model heat_transfer_interface_model [
                density = 1  #kg/m3
                conductivity = [[{aquifer_permeability/fluid_viscosity}, 0, 0],\
                               [0,  {aquifer_permeability/fluid_viscosity}, 0],\
                               [0,  0,  {aquifer_permeability/fluid_viscosity}]]
                capacity = {storage} # [1/Pa]  compressibility of rock
                longitudinal_conductivity = {longitudinal_permeability/fluid_viscosity}
                transversal_conductivity = {transversal_permeability/fluid_viscosity}
                default_opening = {hydraulic_opening} # [m]
                capacity_in_crack = {storage_in_crack} # [1/Pa]
                density_in_crack = 1. # dummy
                use_opening_rate = false
        ]
        """
        # writing the material file
        open("material.dat", "w").write(material_description)
        material_file_name = "material.dat"

    # reading the material file
    aka.parseInput(material_file_name)

    # reading the mesh
    dim = 3
    mesh = aka.Mesh(dim)
    if prank == 0:
        mesh.read(mesh_file)
    mesh.distribute()

    # initialize rock model
    rock_model = aka.SolidMechanicsModelCohesive(mesh)
    rock_model.getElementInserter().addPhysicalSurface("fault")
    rock_model.initFull(_analysis_method=aka._static, _is_extrinsic=False)

    # output number of different elements at different processors
    for ghost_type in [aka._not_ghost, aka._ghost]:
        for el_type in mesh.elementTypes(dim, ghost_type, aka._ek_not_defined):
            nb_el = mesh.getNbElement(el_type, ghost_type)
            print(
                f"prank {prank} {el_type} {ghost_type} - {nb_el} elements", flush=True
            )

    # initialized pressure diffusion model
    flow_model = aka.HeatTransferInterfaceModel(mesh)
    flow_model.initFull(_analysis_method=aka._implicit_dynamic)
    flow_model.setTimeStep(timestep_days * sec_in_day)

    # assign different conductivity values to the cap rock
    low_perm = np.zeros((dim, dim))
    np.fill_diagonal(low_perm, cap_permeability / fluid_viscosity)
    flow_model.assignPropertyToPhysicalGroup(
        "conductivity", "cap_rock", low_perm)

    # dumping
    rock_model.setBaseNameToDumper("solid_mechanics_model", "solid")
    rock_model.setDirectoryToDumper("solid_mechanics_model", para_dir)
    rock_model.addDumpFieldVectorToDumper(
        "solid_mechanics_model", "partitions")
    rock_model.addDumpFieldVectorToDumper(
        "solid_mechanics_model", "displacement")
    rock_model.addDumpFieldVectorToDumper(
        "solid_mechanics_model", "internal_force")
    rock_model.addDumpFieldVectorToDumper(
        "solid_mechanics_model", "external_force")
    rock_model.addDumpFieldToDumper("solid_mechanics_model", "strain")
    rock_model.addDumpFieldToDumper("solid_mechanics_model", "blocked_dofs")
    rock_model.addDumpFieldToDumper("solid_mechanics_model", "stress")

    rock_model.setBaseNameToDumper("cohesive elements", "cohesive")
    rock_model.setDirectoryToDumper("cohesive elements", para_dir)
    rock_model.addDumpFieldVectorToDumper("cohesive elements", "displacement")
    rock_model.addDumpFieldVectorToDumper(
        "cohesive elements", "internal_force")
    rock_model.addDumpFieldVectorToDumper(
        "cohesive elements", "external_force")
    rock_model.addDumpFieldToDumper("cohesive elements", "damage")
    rock_model.addDumpFieldVectorToDumper("cohesive elements", "tractions")
    rock_model.addDumpFieldVectorToDumper("cohesive elements", "opening")
    rock_model.addDumpFieldVectorToDumper(
        "cohesive elements", "contact_opening")
    rock_model.addDumpFieldToDumper(
        "cohesive elements", "contact_opening_normal")
    rock_model.addDumpFieldVectorToDumper(
        "cohesive elements", "contact_tractions")
    rock_model.addDumpFieldToDumper(
        "cohesive elements", "contact_tractions_normal")
    rock_model.addDumpFieldVectorToDumper(
        "cohesive elements", "friction_force")
    rock_model.addDumpFieldToDumper("cohesive elements", "friction_force_norm")
    rock_model.addDumpFieldVectorToDumper("cohesive elements", "normal")
    rock_model.addDumpFieldToDumper("cohesive elements", "blocked_dofs")
    rock_model.addDumpFieldToDumper("cohesive elements", "partitions")
    rock_model.addDumpFieldToDumper("cohesive elements", "residual_sliding")
    rock_model.addDumpFieldToDumper("cohesive elements", "sliding_norm")

    flow_model.setBaseNameToDumper("heat_transfer", "bulk")
    flow_model.setDirectoryToDumper("heat_transfer", para_dir)
    flow_model.addDumpFieldVectorToDumper("heat_transfer", "temperature")
    flow_model.addDumpFieldVectorToDumper(
        "heat_transfer", "external_heat_rate")
    flow_model.addDumpFieldToDumper("heat_transfer", "internal_heat_rate")
    flow_model.addDumpFieldToDumper("heat_transfer", "blocked_dofs")
    flow_model.addDumpFieldToDumper("heat_transfer", "partitions")
    flow_model.addDumpFieldToDumper("heat_transfer", "conductivity")
    flow_model.addDumpFieldToDumper("heat_transfer", "capacity")
    flow_model.addDumpFieldToDumper("heat_transfer", "temperature_on_qpoints")

    flow_model.setBaseNameToDumper("heat_interfaces", "fault")
    flow_model.setDirectoryToDumper("heat_interfaces", para_dir)
    flow_model.addDumpFieldToDumper("heat_interfaces", "temperature")
    flow_model.addDumpFieldVectorToDumper(
        "heat_interfaces", "external_heat_rate")
    flow_model.addDumpFieldVectorToDumper(
        "heat_interfaces", "internal_heat_rate")
    flow_model.addDumpFieldToDumper("heat_interfaces", "opening_on_qpoints")
    flow_model.addDumpFieldToDumper(
        "heat_interfaces", "temperature_on_qpoints_coh")
    flow_model.addDumpFieldToDumper("heat_interfaces", "temperature_gradient")
    flow_model.addDumpFieldToDumper("heat_interfaces", "partitions")

    rock_model.dump("solid_mechanics_model")
    rock_model.dump("cohesive elements")
    flow_model.dump("heat_transfer")
    flow_model.dump("heat_interfaces")

    # set the Dirichlet boundary conditions on solid model
    rock_model.applyBC(aka.FixedValue(0.0, aka._z), "bottom")

    # set overburden + side pressures
    vert_force = np.array([0, 0, -top_depth * rock_density * gravity])
    rock_model.applyBC(aka.FromTraction(vert_force), "top")
    rock_model.applyBC(
        lateralCompressionNormal(
            K_0x=-K0_x,
            K_0y=-K0_y,
            clockwise_rot_angle=clockwise_rotation_angle,
            starting_depth=top_depth,
            model_height=H,
            density=rock_density,
            normal=np.array([1, 0, 0]),
            gravity=gravity,
        ),
        "right",
    )
    rock_model.applyBC(
        lateralCompressionNormal(
            K_0x=-K0_x,
            K_0y=-K0_y,
            clockwise_rot_angle=clockwise_rotation_angle,
            starting_depth=top_depth,
            model_height=H,
            density=rock_density,
            normal=np.array([-1, 0, 0]),
            gravity=gravity,
        ),
        "left",
    )
    rock_model.applyBC(
        lateralCompressionNormal(
            K_0x=-K0_x,
            K_0y=-K0_y,
            clockwise_rot_angle=clockwise_rotation_angle,
            starting_depth=top_depth,
            model_height=H,
            density=rock_density,
            normal=np.array([0, 1, 0]),
            gravity=gravity,
        ),
        "back",
    )
    rock_model.applyBC(
        lateralCompressionNormal(
            K_0x=-K0_x,
            K_0y=-K0_y,
            clockwise_rot_angle=clockwise_rotation_angle,
            starting_depth=top_depth,
            model_height=H,
            density=rock_density,
            normal=np.array([0, -1, 0]),
            gravity=gravity,
        ),
        "front",
    )

    # fix central vertical axis
    nodes_coords = mesh.getNodes()
    dofs_solid = rock_model.getBlockedDOFs()
    for i in range(nodes_coords.shape[0]):
        if (
            nodes_coords[i, 0] <= 0
            and nodes_coords[i, 1] == 0
            and nodes_coords[i, 2] == 0
        ):
            dofs_solid[i, 1] = True
        elif nodes_coords[i, 0] == 0 and nodes_coords[i, 2] == 0:
            dofs_solid[i, 0] = True

    # set the gravity body force
    applyBodyForce(rock_model, gravity)

    # fill in quad points coordinates
    cohesive_mat = rock_model.getMaterial("fault")
    fe_engine = rock_model.getFEEngine("CohesiveFEEngine")
    element_filter = cohesive_mat.getElementFilter()
    quad_coords = cohesive_mat.getInternalReal("quad_coordinates")
    fe_engine.computeIntegrationPointsCoordinates(quad_coords, element_filter)

    # configure the linear algebra solver
    flow_solver = flow_model.getNonLinearSolver()
    flow_solver.set("max_iterations", 10)
    flow_solver.set("threshold", 1e-7)
    flow_solver.set("convergence_type", aka.SolveConvergenceCriteria.residual)

    rock_solver = rock_model.getNonLinearSolver()
    rock_solver.set("max_iterations", 20)
    rock_solver.set("threshold", 1e-6)
    rock_solver.set("convergence_type", aka.SolveConvergenceCriteria.residual)

    # solve for the initial state
    try:
        rock_model.solveStep()
    except Exception:
        error = rock_solver.getError()
        nb_iter = rock_solver.getNbIterations()
        print(
            f"Initial loading didn't converge in {nb_iter} with error = {error}",
            flush=True,
        )
        rock_model.dump("solid_mechanics_model")
        rock_model.dump("cohesive elements")
        flow_model.dump("heat_transfer")
        flow_model.dump("heat_interfaces")
        raise
    nb_iter = rock_solver.getNbIterations()
    if prank == 0:
        print(f"Initial loading converged in {nb_iter}", flush=True)
    rock_model.dump("solid_mechanics_model")
    rock_model.dump("cohesive elements")
    flow_model.dump("heat_transfer")
    flow_model.dump("heat_interfaces")

    # setup Geomechanic solver
    nb_nodes = mesh.getNbNodes()
    solver = AkantuGeomechanicalSolver(
        rock_model,
        flow_model,
        alpha,
        dump_iterations=dump_iterations,
        update_stiffness=update_stiffness,
    )

    it_options = IterativeOptions(
        pcg_maxiterations=30,
        newton_maxiterations=100,
        f_rtol=1e-8,
        f_rock_tol=1e-3,
        f_flow_tol=1e-6,
    )

    # preparing the initial guesses for displacement and pressure
    delta_u_0 = np.zeros([nb_nodes, dim])
    delta_p_0 = np.zeros([nb_nodes, 1])

    # apply injection
    ext_heat = flow_model.getExternalHeatRate()

    # collect all the well nodes
    well_nodes = []
    for id in range(nodes_coords.shape[0]):
        node_coord = nodes_coords[id, :]
        if (
            math.isclose(node_coord[0], 0, abs_tol=1e-5)
            and math.isclose(node_coord[1], 0, abs_tol=1e-5)
            and (node_coord[2] >= (H - h_aq) / 2)
            and (node_coord[2] <= (H + h_aq) / 2)
            and mesh.isLocalOrMasterNode(id)
        ):
            well_nodes.append(id)

    # communicate to get the total number of well nodes
    nb_well_nodes = len(well_nodes)
    nb_well_nodes = comm.allreduce(nb_well_nodes, op=MPI.SUM)
    well_el_len = h_aq / (nb_well_nodes - 1)

    # apply injection: first and last node get half of injection per element.
    inj_per_el = injection_rate / h_aq * well_el_len
    for id in well_nodes:
        node_coord = nodes_coords[id, :]
        if math.isclose(node_coord[2], (H - h_aq) / 2, abs_tol=1e-5) or math.isclose(
            node_coord[2], (H + h_aq) / 2, abs_tol=1e-5
        ):
            ext_heat[id] = inj_per_el / 2
        else:
            ext_heat[id] = inj_per_el
        print(f"Injection applied on node {id} {prank=}", flush=True)

    coh_filter = element_filter(coh_type, aka._not_ghost)
    nb_coh_elem = coh_filter.size
    nb_quads_per_coh = fe_engine.getNbIntegrationPoints(coh_type)
    coh_quad_coords = quad_coords(coh_type)
    plastic_slip = cohesive_mat.getInternalReal("residual_sliding")(coh_type)
    friction = cohesive_mat.getInternalReal("friction_force")(coh_type)
    normal_force_normal = cohesive_mat.getInternalReal("contact_tractions_normal")(
        coh_type
    )
    normals = cohesive_mat.getInternalReal("normal")(coh_type)
    total_slip = cohesive_mat.getInternalReal("sliding_norm")(coh_type)
    coh_conn = mesh.getConnectivity(coh_type, aka._not_ghost)

    # track cohesive elements having at least one node along fault line on the front face
    els_on_fault_line = []
    for el in range(nb_coh_elem):
        coh_nodes = coh_conn[el]
        for node in coh_nodes:
            node_coord = nodes_coords[node]
            if math.isclose(node_coord[1], 0, abs_tol=1e-5):
                els_on_fault_line.append(el)
                break
    els_on_fault_line = np.array(els_on_fault_line)
    output_els_found = True
    if len(els_on_fault_line) == 0:
        output_els_found = False

    # fill in the quad coords array and prepare output for res_slip
    quad_coords_output = np.zeros(
        [len(els_on_fault_line) * nb_quads_per_coh, dim])
    quad_ids = np.zeros(len(els_on_fault_line) * nb_quads_per_coh, dtype=int)
    for quad in range(nb_quads_per_coh):
        quad_ids[quad::nb_quads_per_coh] = els_on_fault_line * \
            nb_quads_per_coh + quad
    quad_normals_output = np.zeros(
        [len(els_on_fault_line) * nb_quads_per_coh, dim])
    plastic_slip_output = np.zeros([len(quad_ids), nb_timesteps + 1])
    friction_output = np.zeros([len(quad_ids) * dim, nb_timesteps + 1])
    normal_force_output = np.zeros([len(quad_ids), nb_timesteps + 1])
    total_slip_output = np.zeros([len(quad_ids), nb_timesteps + 1])

    if output_els_found:
        quad_coords_output = coh_quad_coords[quad_ids]
        quad_normals_output = normals[quad_ids].ravel()
        plastic_slip_output[:, 0] = plastic_slip[quad_ids, 0]
        friction_output[:, 0] = friction[quad_ids].ravel()
        normal_force_output[:, 0] = normal_force_normal[quad_ids, 0]
        total_slip_output[:, 0] = total_slip[quad_ids, 0]

    # output for a parallel execution
    total_quad_nb = comm.reduce(len(quad_ids), op=MPI.SUM, root=0)
    sendcounts_ip_scalar = np.array(
        comm.gather(plastic_slip_output.size, root=0))
    sendcounts_ip_coords = np.array(
        comm.gather(quad_coords_output.size, root=0))
    sendcounts_ip_vector = np.array(comm.gather(friction_output.size, root=0))
    full_quad_coords = None
    full_quad_normals = None
    full_plastic_slip = None
    full_friction = None
    full_normal_force = None
    full_total_slip = None
    if prank == 0:
        full_quad_coords = np.empty([total_quad_nb, dim], dtype=float)
        full_quad_normals = np.empty([total_quad_nb, dim], dtype=float)
        full_plastic_slip = np.empty(
            [total_quad_nb, nb_timesteps + 1], dtype=float)
        full_friction = np.empty(
            [total_quad_nb * dim, nb_timesteps + 1], dtype=float)
        full_normal_force = np.empty(
            [total_quad_nb, nb_timesteps + 1], dtype=float)
        full_total_slip = np.empty(
            [total_quad_nb, nb_timesteps + 1], dtype=float)

    # prepare nodes and empty arrays for storing data for comparison with analytics
    upper_fault_nodes = mesh.getElementGroup(
        "fault").getNodeGroup().getNodes()[:, 0]
    upper_output_nodes = []
    for node in upper_fault_nodes:
        if math.isclose(nodes_coords[node, 1], 0, abs_tol=1e-5):
            if mesh.isLocalOrMasterNode(node):
                upper_output_nodes.append(node)
    upper_output_nodes = np.array(upper_output_nodes)
    output_nodes_found = True
    if len(upper_output_nodes) == 0:
        output_nodes_found = False
    pressure_output = np.zeros([len(upper_output_nodes), nb_timesteps + 1])
    output_node_coords = np.zeros([len(upper_output_nodes), dim])
    if output_nodes_found:
        output_node_coords = nodes_coords[upper_output_nodes]

    # filling in the initial values
    if output_nodes_found:
        pressure_output[:, 0] = flow_model.getTemperature()[
            upper_output_nodes].ravel()

    # output for a parallel execution
    total_node_nb = comm.reduce(len(upper_output_nodes), op=MPI.SUM, root=0)
    sendcounts_nodal_scalar = np.array(
        comm.gather(pressure_output.size, root=0))
    sendcounts_nodal_coords = np.array(
        comm.gather(output_node_coords.size, root=0))
    full_pressure = None
    full_node_coords = None
    if prank == 0:
        full_pressure = np.empty(
            [total_node_nb, nb_timesteps + 1], dtype=float)
        full_node_coords = np.empty([total_node_nb, dim], dtype=float)

    # dump distances only once
    comm.Gatherv(
        sendbuf=output_node_coords,
        recvbuf=(full_node_coords, sendcounts_nodal_coords),
        root=0,
    )
    comm.Gatherv(
        sendbuf=quad_coords_output,
        recvbuf=(full_quad_coords, sendcounts_ip_coords),
        root=0,
    )
    comm.Gatherv(
        sendbuf=quad_normals_output,
        recvbuf=(full_quad_normals, sendcounts_ip_coords),
        root=0,
    )
    if prank == 0:
        np.save(os.path.join(output_path, "node_coords.npy"), full_node_coords)
        np.save(os.path.join(output_path, "quad_coords.npy"), full_quad_coords)
        np.save(os.path.join(output_path, "quad_normals.npy"), full_quad_normals)

    # time stepping
    for i in range(nb_timesteps):
        if prank == 0:
            print(f"\nSolving time step {i}", flush=True)

        cvged = solver.solve_step(delta_u_0, delta_p_0, it_options)
        if not cvged:
            rock_model.assembleInternalForces()
            flow_model.assembleInternalHeatRate()
            rock_model.dump("solid_mechanics_model")
            rock_model.dump("cohesive elements")
            flow_model.dump("heat_transfer")
            flow_model.dump("heat_interfaces")
            raise Exception("Time step solver did not converge")

        # dumping solutions for plotting
        rock_model.assembleInternalForces()
        flow_model.assembleInternalHeatRate()
        rock_model.dump("solid_mechanics_model")
        rock_model.dump("cohesive elements")
        flow_model.dump("heat_transfer")
        flow_model.dump("heat_interfaces")

        # store plastic slip
        if output_els_found:
            plastic_slip_output[:, i + 1] = plastic_slip[quad_ids, 0]
            friction_output[:, i + 1] = friction[quad_ids].ravel()
            normal_force_output[:, i + 1] = normal_force_normal[quad_ids, 0]
            total_slip_output[:, i + 1] = total_slip[quad_ids, 0]

        # store pressure and opening profiles along the x axis
        if output_nodes_found:
            pressure_output[:, i + 1] = flow_model.getTemperature()[
                upper_output_nodes
            ].ravel()

        # gather output at root 0
        comm.Gatherv(
            sendbuf=pressure_output,
            recvbuf=(full_pressure, sendcounts_nodal_scalar),
            root=0,
        )
        comm.Gatherv(
            sendbuf=plastic_slip_output,
            recvbuf=(full_plastic_slip, sendcounts_ip_scalar),
            root=0,
        )
        comm.Gatherv(
            sendbuf=friction_output,
            recvbuf=(full_friction, sendcounts_ip_vector),
            root=0,
        )
        comm.Gatherv(
            sendbuf=normal_force_output,
            recvbuf=(full_normal_force, sendcounts_ip_scalar),
            root=0,
        )
        comm.Gatherv(
            sendbuf=total_slip_output,
            recvbuf=(full_total_slip, sendcounts_ip_scalar),
            root=0,
        )

        # output pressure, sliding and distances
        if prank == 0:
            np.save(os.path.join(output_path, "pressure.npy"), full_pressure)
            np.save(os.path.join(output_path, "friction.npy"), full_friction)
            np.save(os.path.join(output_path, "normal_force.npy"),
                    full_normal_force)
            np.save(os.path.join(output_path, "total_slip.npy"), full_total_slip)
            np.save(os.path.join(output_path, "plastic_slip.npy"),
                    full_plastic_slip)
            np.save(
                os.path.join(output_path, "times.npy"),
                np.arange(stop=(i + 2) * timestep_days, step=timestep_days),
            )

    print(f"{prank=} simulation completed")


# -----------------------------------------------------------------
if __name__ == "__main__":
    main()
