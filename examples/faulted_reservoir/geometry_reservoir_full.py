import gmsh
import numpy as np

# occmetric parameters
el_size = 75
aq_el_size = el_size / 3
H = 2000
W = 2000
D = 2000
h_aq = 100
h_cap = 150
W_fault = 900
fault_angle = np.pi / 18

gmsh.initialize()
gmsh.model.add("reservoir")

layer_thicknesses = [
    H / 2 - h_aq / 2 - h_cap,
    h_cap,
    h_aq,
    h_cap,
    H / 2 - h_aq / 2 - h_cap,
]
vert_coords_left = np.cumsum(layer_thicknesses)
shift = 130
layer_thicknesses[0] += shift
layer_thicknesses[-1] -= shift
vert_coords_right = np.cumsum(layer_thicknesses)
# first layer
gmsh.model.occ.addPoint(0, 0, 0, el_size, 1)
gmsh.model.occ.addPoint(W_fault, 0, 0, el_size, 2)
gmsh.model.occ.addPoint(W, 0, 0, el_size, 3)
gmsh.model.occ.addPoint(0, 0, vert_coords_left[0], el_size, 4)
gmsh.model.occ.addPoint(
    W_fault + vert_coords_left[0] * np.tan(fault_angle),
    0,
    vert_coords_left[0],
    el_size,
    5,
)
gmsh.model.occ.addPoint(
    W_fault + vert_coords_right[0] * np.tan(fault_angle),
    0,
    vert_coords_right[0],
    el_size,
    6,
)
gmsh.model.occ.addPoint(W, 0, vert_coords_right[0], el_size, 7)
gmsh.model.occ.addPoint(-W, 0, 0, el_size, 31)
gmsh.model.occ.addPoint(-W, 0, vert_coords_left[0], el_size, 32)
gmsh.model.occ.addLine(1, 2, 1)
gmsh.model.occ.addLine(2, 3, 2)
gmsh.model.occ.addLine(1, 4, 3)
gmsh.model.occ.addLine(2, 5, 4)
gmsh.model.occ.addLine(3, 7, 5)
gmsh.model.occ.addLine(4, 5, 6)
gmsh.model.occ.addLine(5, 6, 7)
gmsh.model.occ.addLine(6, 7, 8)
gmsh.model.occ.addLine(31, 1, 41)
gmsh.model.occ.addLine(32, 31, 42)
gmsh.model.occ.addLine(4, 32, 43)

gmsh.model.occ.addCurveLoop([1, 4, -6, -3], 1)
gmsh.model.occ.addPlaneSurface([1], 1)
gmsh.model.occ.addCurveLoop([2, 5, -8, -7, -4], 2)
gmsh.model.occ.addPlaneSurface([2], 2)
gmsh.model.occ.addCurveLoop([3, 43, 42, 41], 101)
gmsh.model.occ.addPlaneSurface([101], 101)

# two layers on the left and one layer on the right
gmsh.model.occ.addPoint(0, 0, vert_coords_left[1], aq_el_size, 8)
gmsh.model.occ.addPoint(
    W_fault + vert_coords_left[1] * np.tan(fault_angle),
    0,
    vert_coords_left[1],
    aq_el_size,
    9,
)
gmsh.model.occ.addPoint(0, 0, vert_coords_left[2], aq_el_size, 10)
gmsh.model.occ.addPoint(
    W_fault + vert_coords_left[2] * np.tan(fault_angle),
    0,
    vert_coords_left[2],
    aq_el_size,
    11,
)
gmsh.model.occ.addPoint(
    W_fault + vert_coords_right[1] * np.tan(fault_angle),
    0,
    vert_coords_right[1],
    aq_el_size,
    12,
)
gmsh.model.occ.addPoint(W, 0, vert_coords_right[1], aq_el_size, 13)
gmsh.model.occ.addPoint(-W, 0, vert_coords_left[1], aq_el_size, 33)
gmsh.model.occ.addPoint(-W, 0, vert_coords_left[2], aq_el_size, 34)
gmsh.model.occ.addLine(7, 13, 9)
gmsh.model.occ.addLine(4, 8, 10)
gmsh.model.occ.addLine(8, 9, 11)
gmsh.model.occ.addLine(6, 9, 12)
gmsh.model.occ.addLine(9, 11, 13)
gmsh.model.occ.addLine(11, 12, 14)
gmsh.model.occ.addLine(8, 10, 15)
gmsh.model.occ.addLine(10, 11, 16)
gmsh.model.occ.addLine(12, 13, 17)
gmsh.model.occ.addLine(33, 32, 44)
gmsh.model.occ.addLine(8, 33, 45)
gmsh.model.occ.addLine(34, 33, 46)
gmsh.model.occ.addLine(10, 34, 47)
gmsh.model.occ.addCurveLoop([6, 7, 12, -11, -10], 3)
gmsh.model.occ.addPlaneSurface([3], 3)
gmsh.model.occ.addCurveLoop([11, 13, -16, -15], 4)
gmsh.model.occ.addPlaneSurface([4], 4)
gmsh.model.occ.addCurveLoop([8, 9, -17, -14, -13, -12], 5)
gmsh.model.occ.addPlaneSurface([5], 5)
gmsh.model.occ.addCurveLoop([10, 45, 44, -43], 102)
gmsh.model.occ.addPlaneSurface([102], 102)
gmsh.model.occ.addCurveLoop([15, 47, 46, -45], 103)
gmsh.model.occ.addPlaneSurface([103], 103)

# one layer on the left and on the right
gmsh.model.occ.addPoint(0, 0, vert_coords_left[3], el_size, 16)
gmsh.model.occ.addPoint(
    W_fault + vert_coords_left[3] * np.tan(fault_angle),
    0,
    vert_coords_left[3],
    el_size,
    17,
)
gmsh.model.occ.addPoint(
    W_fault + vert_coords_right[2] * np.tan(fault_angle),
    0,
    vert_coords_right[2],
    aq_el_size,
    14,
)
gmsh.model.occ.addPoint(W, 0, vert_coords_right[2], aq_el_size, 15)
gmsh.model.occ.addPoint(-W, 0, vert_coords_left[3], el_size, 35)
gmsh.model.occ.addLine(13, 15, 18)
gmsh.model.occ.addLine(12, 14, 19)
gmsh.model.occ.addLine(14, 17, 20)
gmsh.model.occ.addLine(10, 16, 21)
gmsh.model.occ.addLine(16, 17, 22)
gmsh.model.occ.addLine(14, 15, 23)
gmsh.model.occ.addLine(16, 35, 48)
gmsh.model.occ.addLine(35, 34, 49)

gmsh.model.occ.addCurveLoop([16, 14, 19, 20, -22, -21], 6)
gmsh.model.occ.addPlaneSurface([6], 6)
gmsh.model.occ.addCurveLoop([17, 18, -23, -19], 7)
gmsh.model.occ.addPlaneSurface([7], 7)
gmsh.model.occ.addCurveLoop([21, 48, 49, -47], 104)
gmsh.model.occ.addPlaneSurface([104], 104)

# top cap layer on the right
gmsh.model.occ.addPoint(
    W_fault + vert_coords_right[3] * np.tan(fault_angle),
    0,
    vert_coords_right[3],
    el_size,
    18,
)
gmsh.model.occ.addPoint(W, 0, vert_coords_right[3], el_size, 19)
gmsh.model.occ.addLine(15, 19, 24)
gmsh.model.occ.addLine(17, 18, 25)
gmsh.model.occ.addLine(18, 19, 26)

gmsh.model.occ.addCurveLoop([23, 24, -26, -25, -20], 8)
gmsh.model.occ.addPlaneSurface([8], 8)

# one layer on the left and on the right
gmsh.model.occ.addPoint(0, 0, H, el_size, 20)
gmsh.model.occ.addPoint(W_fault + H * np.tan(fault_angle), 0, H, el_size, 21)
gmsh.model.occ.addPoint(W, 0, H, el_size, 22)
gmsh.model.occ.addPoint(-W, 0, H, el_size, 36)
gmsh.model.occ.addLine(19, 22, 27)
gmsh.model.occ.addLine(16, 20, 28)
gmsh.model.occ.addLine(20, 21, 29)
gmsh.model.occ.addLine(18, 21, 30)
gmsh.model.occ.addLine(21, 22, 31)
gmsh.model.occ.addLine(20, 36, 50)
gmsh.model.occ.addLine(36, 35, 51)

gmsh.model.occ.addCurveLoop([22, 25, 30, -29, -28], 9)
gmsh.model.occ.addPlaneSurface([9], 9)
gmsh.model.occ.addCurveLoop([26, 27, -31, -30], 10)
gmsh.model.occ.addPlaneSurface([10], 10)
gmsh.model.occ.addCurveLoop([28, 50, 51, -48], 105)
gmsh.model.occ.addPlaneSurface([105], 105)

ov = gmsh.model.occ.extrude(
    [
        (2, 1),
        (2, 2),
        (2, 3),
        (2, 4),
        (2, 5),
        (2, 6),
        (2, 7),
        (2, 8),
        (2, 9),
        (2, 10),
        (2, 101),
        (2, 102),
        (2, 103),
        (2, 104),
        (2, 105),
    ],
    0,
    D,
    0,
)
ov = gmsh.model.occ.extrude(
    [
        (2, 1),
        (2, 2),
        (2, 3),
        (2, 4),
        (2, 5),
        (2, 6),
        (2, 7),
        (2, 8),
        (2, 9),
        (2, 10),
        (2, 101),
        (2, 102),
        (2, 103),
        (2, 104),
        (2, 105),
    ],
    0,
    -D,
    0,
)
gmsh.model.occ.synchronize()

# add horizontal injection well on the front face
# see https://gmsh.info/doc/cookbook/occmetry/embed-point.html

# add physical groups
gmsh.model.addPhysicalGroup(
    3, [1, 2, 16, 17, 11, 26, 15, 9, 24, 10, 25, 30], name="bottom_top"
)
gmsh.model.addPhysicalGroup(
    3, [3, 5, 6, 8, 27, 12, 18, 20, 23, 21, 29, 14], name="cap_rock"
)
gmsh.model.addPhysicalGroup(3, [4, 7, 19, 22, 28, 13], name="aquifer")
gmsh.model.addPhysicalGroup(
    2, [148, 152, 155, 158, 161, 205, 209, 212, 215, 218], name="left"
)
gmsh.model.addPhysicalGroup(
    2, [112, 124, 133, 136, 144, 169, 181, 190, 193, 201], name="right"
)
gmsh.model.addPhysicalGroup(2, [106, 111, 149, 163, 168, 206], name="bottom")
gmsh.model.addPhysicalGroup(2, [141, 145, 160, 198, 202, 217], name="top")
gmsh.model.addPhysicalGroup(
    2,
    [110, 119, 123, 132, 143, 150, 153, 156, 159, 162, 115, 127, 135, 139, 146],
    name="back",
)
gmsh.model.addPhysicalGroup(
    2,
    [207, 210, 213, 216, 219, 167, 176, 180, 189, 200, 172, 184, 192, 196, 203],
    name="front",
)
gmsh.model.addPhysicalGroup(
    2,
    [
        164,
        171,
        173,
        177,
        183,
        185,
        186,
        195,
        197,
        107,
        114,
        116,
        120,
        126,
        128,
        129,
        138,
        140,
    ],
    name="fault",
)

gmsh.model.occ.synchronize()
gmsh.model.mesh.generate(3)
gmsh.write("faulted_reservoir_full_dense.msh")
# gmsh.write("faulted_reservoir_full.geo_unrolled")
