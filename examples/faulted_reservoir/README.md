### Demonstration of the geomechanical simulator on the case of injection into a faulted aquifer

*reservoir_injection.py* runs the simulation. The code has some optional arguments to be provided. The list of the arguments can be consulted by running the following lines::

        python reservoir_injection.py --help

The code can be run on a single processor and in parallel. On a single processor the command to run with default parameters is::

        python reservoir_injection.py

When running in parallel, following lines should be run::

        mpiexec -np N python reservoir_injection.py --material_file material.dat

In the above code, N is the number of processors to be employed. By default, the code will run simulation on the coarse mesh faulted_reservoir_coarse.msh with fixed elastic stiffness during Newton iterations. When running in parallel, providing a material file is mandatory.

A parallel job with changed parameters can be started as

        mpiexec -np N python reservoir_injection.py --output_folder folder_name --mesh_file faulted_reservoir.msh --time_steps X --material_file material.dat --rotation_angle M

where *N* is the number of processes, *X* is the number of time steps, and *M* is the value of the rotation angle of stress tensor in clock-wise direction. The code will create the output folder if it doesn't yet exist. It will store paraview output files, geomechanical parameters (pressure, stress, slip) along the central dip line along the fault and the time value for each time in this folder.

*geometry_reservoir_full.py* constructs mesh of the model. Current state of the file generates a dense mesh, which was used to run the simulation presented in the paper.

*faulted_reservoir_coarse.msh* is the coarse mesh that can be used to test the work of the simulator.

To reproduce the results, presented in the paper, the user can launch *reproduce.sh*. This will start parallel execution of the code on 72 cores. A corresponding cluster/machine is required.
