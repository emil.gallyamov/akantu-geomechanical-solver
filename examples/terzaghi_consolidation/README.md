### Benchmarking the geomechanical simulator with Terzaghi consolidation problem.

*terzaghi_consolidation.ipynb* is the Jupyter Notebook which explains the simulation steps and compares the simulated results to the analytical solution.
