block_height = 40;
ext_square_side = 40;
ext_circle_r = 5;
el_size = 0.05;
far_el_size = ext_square_side / 10;

// Outer Box domain
Point(1) = {-ext_square_side/2, -ext_square_side/2, 0, far_el_size};
Point(2) = {ext_square_side/2, -ext_square_side/2, 0, far_el_size};
Point(3) = {ext_square_side/2, ext_square_side/2, 0, far_el_size};
Point(4) = {-ext_square_side/2, ext_square_side/2, 0, far_el_size};
Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 1};

// Surface between square and external circle
Curve Loop(1) = {1,2,3,4};

// Points that make up the external circle
Point(5) = {0,0,0, el_size};
Point(6) = {-ext_circle_r,0,0, el_size};
Point(7) = {0,-ext_circle_r,0, el_size};
Point(8) = {ext_circle_r,0,0, el_size};
Point(9) = {0,ext_circle_r,0, el_size};

// Curves that connect points defining circle
Circle(5) = {6,5,7};
Circle(6) = {7,5,8};
Circle(7) = {8,5,9};
Circle(8) = {9,5,6};
// specify closed loop to make surface
Curve Loop(2) = {5,6,7,8};
Surface(1) = {1,2};

// internal surfaces
Line(9) = {5,6};
Line(10) = {5,7};
Line(11) = {5,8};
Line(12) = {5,9};

Curve Loop(4) = {9,5,-10};
Curve Loop(5) = {10,6,-11};
Curve Loop(6) = {11,7,-12};
Curve Loop(7) = {12,8,-9};

Surface(2) = {4};
Surface(3) = {5};
Surface(4) = {6};
Surface(5) = {7};

// upper part of the block
Translate{ 0.0, 0.0,  block_height/2} { Duplicata{Line{1,2,3,4};}  }
Line(21) = {1,10};
Line(22) = {2,11};
Line(23) = {3,15};
Line(24) = {4,19};

Curve Loop(8) = {13, 14, 15, 16};
Curve Loop(9) = {13, -22, -1, 21};
Curve Loop(10) = {14, -23, -2, 22};
Curve Loop(11) = {15, -24, -3, 23};
Curve Loop(12) = {16, -21, -4, 24};

Surface(7) = {8};
Surface(8) = {9};
Surface(9) = {10};
Surface(10) = {11};
Surface(11) = {12};

Surface Loop(1) = {1,2,3,4,5,7,8,9,10,11};
Volume(1) = {1};

// lower part of the block
Translate{ 0.0, 0.0,  -block_height/2} { Duplicata{Line{1,2,3,4};}  }
Line(29) = {29,4};
Line(30) = {20,1};
Line(31) = {21,2};
Line(32) = {25,3};

Curve Loop(13) = {25,26,27,28};
Curve Loop(14) = {25, 31, -1, -30};
Curve Loop(15) = {26, 32, -2, -31};
Curve Loop(16) = {27, 29, -3, -32};
Curve Loop(17) = {28, 30, -4, -29};

Surface(12) = {13};
Surface(13) = {14};
Surface(14) = {15};
Surface(15) = {16};
Surface(16) = {17};

Surface Loop(2) = {1,2,3,4,5,12,13,14,15,16};
Volume(2) = {2};

// physical entities
Physical Surface("fault") = {1,2,3,4,5};
Physical Surface("left_down") = {16};
Physical Surface("left_up") = {11};
Physical Surface("right_down") = {14};
Physical Surface("right_up") = {9};
Physical Surface("front_down") = {13};
Physical Surface("front_up") = {8};
Physical Surface("back_down") = {15};
Physical Surface("back_up") = {10};
Physical Surface("bottom") = {12};
Physical Surface("top") = {7};
Physical Volume("top") = {1};
Physical Volume("bottom") = {2};
