### Benchmarking the geomechanical simulator with injection into a sheared fault.

*injection_sheared_circular_fault.py* runs the simulation. The code has some optional arguments to be provided. The list of the arguments can be consulted by running the following lines::

        python injection_sheared_circular_fault.py --help

The code can be run on a single processor and in parallel. On a single processor the command to run with default parameters is::

        python injection_sheared_circular_fault.py

When running in parallel, following lines should be run::

        mpiexec -np N python injection_sheared_circular_fault.py --material_file material.dat

In the above code, N is the number of processors to be employed. By default, the code will run the marginally pressurized case on the circular_slip_coarse.msh mesh with fixed elastic stiffness during Newton. When running in parallel, providing a material file is mandatory.

A parallel job with changed parameters can be started as

        mpiexec -np N python injection_sheared_circular_fault.py --output_folder folder_name --mesh_file sphere_coarse.msh --time_steps X --material_file material.dat

where *N* is the number of processes and *X* is the number of time steps. The code will create the output folder if it doesn't yet exist. It will store paraview output files, geomechanical parameters (pressure, stress, slip) along the *y*-axis and the time value for each time in this folder.

*circular_slip_unstructured.geo* contains the geometrical model of the 3D block with embedded fault passing through its centre. Current state of the file corresponds to the mesh employed to produce the results in the paper. To generate an .msh file, one has to run the following command::

        gmsh -3 circular_slip_unstructured.geo -o circular_slip.msh

*plotting_injection_fault.py* is the file plotting the pressure, slip, friction and normal traction on the fault along the *y*-axis. It takes the data from the output folder.
