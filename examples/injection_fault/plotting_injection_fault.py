import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import os
import sys
sys.path.append(os.path.join(os.path.join(os.path.join(
    sys.path[0], '..'), '..'), 'analytical_solutions'))
import analytical_solutions as an_sol

# loads
inj_rate = 0.000001  # [m3/sec] = 0.06 liter/min
critically_stressed = False
if critically_stressed:
    shear_load = 47999575.056301944  # [Pa] critically stressed
    final_time = 120  # [sec] critically stressed
else:
    shear_load = 47830022.52077785  # [Pa] marginally stressed
    final_time = 5400  # [sec] marginally pressurized

vert_load = 80e6  # [Pa]
times = np.linspace(0, final_time, 100)
bem_time_steps = 100  # sec
bem_timestep = final_time / bem_time_steps
plot_times = [final_time]  # seconds
pl_threshold = 1e-12
pressure_threshold = 0.017

# material parameters
dim = 3
E = 60e9  # Young modulus (Pa)
poisson = 0.
mu = E / 2 / (1 + poisson)  # shear modulus
friction_coef = 0.6

alpha = 0  # Biot's coefficient
kh = 1e-15  # [m3] fault transmissibility (k * h)
# [m] hydraulic aperture from cubic law kh = w^3/12
default_opening = (kh * 12)**(1. / 3)
longitudinal_permeability = kh / \
    default_opening  # [m2] intrinsic permeability
# [m2 /s] fracture hydraulic diffusivity = perm / storage / viscosity
diffusivity = 1e-3
fluid_viscosity = 8.9e-4  # [Pa s]  water viscosity from wikipedia
# [1/Pa] combined effect of fluid compressibility and fault porosity
storage_in_crack = longitudinal_permeability / diffusivity / fluid_viscosity

# collecting coefficients for fundamental solution
alpha_crack = longitudinal_permeability / storage_in_crack / fluid_viscosity
injection_scaled_crack = - inj_rate * fluid_viscosity / \
    longitudinal_permeability / default_opening


delta_p = an_sol.delta_p_star(
    Qw=inj_rate, eta=fluid_viscosity, k=longitudinal_permeability, wh=default_opening)
T_param = an_sol.T_parameter(
    tau_0=shear_load, f=friction_coef, sigma_0=vert_load, delta_p_star=delta_p)
if critically_stressed:
    lambda_ = an_sol.lambda_crit_stressed(T_param)
else:
    lambda_ = an_sol.lambda_marg_pressurized(T_param)
lambda_viesca = an_sol.lambda_analytical(T_param)
print(f'{T_param=} {lambda_=} {delta_p=} {default_opening=}')

# extract data from numerical simulations
friction_penalty_mult = 10
friction_penalty = friction_penalty_mult * E  # Pa
compr_penalty_mult = 100
compr_penalty = compr_penalty_mult * E

pressures = []
slips = []
slips_rel = []
node_distances = []
slips_plastic = []
contact_tractions = []
contact_openings = []
openings = []
friction_forces = []
pressures_quads = []
quad_distances = []
output_times = []
ordered_nodes = []
field_names = ['pressure', 'pressure_quads', 'sliding', 'distances',
               'plastic_slip', 'contact_openings', 'contact_tractions', 'friction_force', 'openings', 'quad_coords', 'times']

# first load the node distances file to later order all the arrays
dist_name = 'output/distances.npy'
dist = np.load(dist_name)
ordered_nodes = np.argsort(dist)
# also load the quad coords
quad_coords_file = 'output/quad_coords.npy'
quad_coords = np.load(quad_coords_file)
ordered_quads = np.argsort(quad_coords[:, 0])

for field_name, list_ in zip(field_names, [pressures, pressures_quads, slips, node_distances, slips_plastic, contact_openings, contact_tractions, friction_forces, openings, quad_distances, output_times]):
    full_name = './output/' + field_name + '.npy'
    field = np.load(full_name)
    # order arrays
    if field_name in ['pressure', 'sliding', 'distances']:
        field = field[ordered_nodes]
    elif field_name in ['plastic_slip', 'pressure_quads']:
        field = field[ordered_quads]
    elif field_name in ['contact_openings', 'contact_tractions', 'friction_force', 'openings']:
        field = field.transpose().reshape((-1, len(ordered_quads), dim))
        field = field[:, ordered_quads]
    elif field_name == 'quad_coords':
        quad_coordinates = field[ordered_quads]
        field = np.linalg.norm(quad_coordinates, axis=1)
        negative_quad_indices = quad_coordinates[:, 0] < 0
        field[negative_quad_indices] *= -1
    list_.append(field)
    # fill in relative sliding
    if field_name == 'sliding':
        init_slip = shear_load / friction_penalty
        slips_rel.append(field - init_slip)

# results plotting
fig1, (ax1, ax2) = plt.subplots(2)
fig4, (ax6, ax7) = plt.subplots(2)
fig7, (ax10, ax11) = plt.subplots(2, layout="constrained")

fig1.suptitle('Pressure along the fault')
fig4.suptitle(
    r"Maximum friction force $\mu \sigma_n'$ and the actual one $\tau_x$")

prop_cycle = plt.rcParams['axes.prop_cycle']
colors = prop_cycle.by_key()['color']
lines = ["-", "--", "-.", ":", "-", "--", "-.", ":"]
markers = ['o', '*', 'v', '^', 's', 'o', '*', 'v', '^', 's']

# plotting the solutions
for plot_time, color in zip(plot_times, colors):

    # analytic solution
    r = np.linspace(0, 10, num=100)
    an_pres = an_sol.pressure_diffusion_2D(
        r=r, alpha=alpha_crack, inj_rate_scaled=injection_scaled_crack, time=plot_time)
    diffusion_front = an_sol.diffusion_front(
        k=longitudinal_permeability, S=storage_in_crack, eta=fluid_viscosity, time=plot_time)
    # track the position of the diffusion front from pressure profile
    fem_timestep = output_times[0][1] - output_times[0][0]
    plot_step = int(plot_time / fem_timestep)

    rupture_front_an = diffusion_front * lambda_
    if critically_stressed:
        an_slip = an_sol.slip_crit_stressed(mu=mu, diffusion_front=diffusion_front, T=T_param, f=friction_coef, delta_p=delta_p,
                                            distances=r, rupture_front=rupture_front_an)
        an_slip_viesca = an_sol.complete_slip_profile_cs(
            G=mu, diffusion_front=diffusion_front, T=T_param, f=friction_coef, dp_star=delta_p, distances=r, lambda_analytical=an_sol.lambda_analytical)
    else:
        an_slip = an_sol.slip_marg_pressurized(mu=mu, f=friction_coef, delta_p=delta_p,
                                               distances=r, rupture_front=rupture_front_an)
        an_slip_viesca = an_sol.complete_slip_profile_mp(
            G=mu, diffusion_front=diffusion_front, T=T_param, f=friction_coef, dp_star=delta_p, distances=r, lambda_analytical=an_sol.lambda_analytical)

    ax1.plot(r, an_pres, color='k',
             linestyle='--', label=r'$P_{an}$')
    ax1.plot(-np.flip(r), np.flip(an_pres), color='k', ls='--')
    ax10.plot(r, an_pres, color='k',
              linestyle='--', label=r'$P_{an}$')
    ax10.plot(-np.flip(r), np.flip(an_pres), color='k', ls='--')
    ax11.plot(r, an_slip_viesca, color='k',
              linestyle='--', label=r'$\delta_{s,an}$')
    ax11.plot(-np.flip(r), np.flip(an_slip_viesca),
              color='k', linestyle='--')

    pres_max = 0
    slip_rel_max = 0
    slip_pl_max = 0
    for pressure, pressure_quads, slip_rel, slip_plastic, contact_opening, contact_traction, friction_force, opening, distance_node, quad_dist, output_time, linestyle, marker, color in zip(pressures, pressures_quads, slips_rel, slips_plastic, contact_openings, contact_tractions, friction_forces, openings, node_distances, quad_distances, output_times, lines, markers, colors):
        # check if fem solution exists
        fem_exists = True
        fem_timestep = output_time[1] - output_time[0]
        plot_step = int(plot_time / fem_timestep)
        if plot_step > len(output_time) + 1:
            fem_exists = False

        if fem_exists:
            ax1.scatter(distance_node,
                        pressure[:, plot_step], label=r'Nodal P_{num}')
            ax1.scatter(
                quad_dist, pressure_quads[:, plot_step], label=r'Quad P_{num}')
            an_pres_nodes = an_sol.pressure_diffusion_2D(
                r=distance_node, alpha=alpha_crack, inj_rate_scaled=injection_scaled_crack, time=plot_time)
            ax2.scatter(distance_node,
                        abs(pressure[:, plot_step] - an_pres_nodes), linestyle=linestyle, label='err nodes')
            an_pres_quads = an_sol.pressure_diffusion_2D(
                r=quad_dist, alpha=alpha_crack, inj_rate_scaled=injection_scaled_crack, time=plot_time)
            ax2.scatter(quad_dist,
                        abs(pressure_quads[:, plot_step] - an_pres_quads), s=5, label='err quads')
            pres_max = max(pres_max, pressure.max())
            ax6.scatter(quad_dist,
                        abs(friction_force[plot_step, :, 0]), marker=marker, s=25, label=rf"$\tau_x$ num")
            ax6.scatter(quad_dist,
                        abs(contact_traction[plot_step, :, 2]) * friction_coef, color='k', s=5, label=rf"$\mu \sigma_n'$ num")
            ax7.scatter(quad_dist,
                        (abs(contact_traction[plot_step, :, 2]) * friction_coef - abs(friction_force[plot_step, :, 0])) / (abs(contact_traction[plot_step, :, 2]) * friction_coef) * 100, marker=marker, s=10)
            ax10.plot(distance_node,
                      pressure[:, plot_step], color=color, linestyle=linestyle, label=r'$P^{num}$')
            ax11.scatter(quad_dist,
                         abs(opening[plot_step, :, 0] + shear_load / friction_penalty), marker=marker, s=10, label=r"$\delta_{s,num}$")


ax1.xaxis.set_tick_params(labelbottom=False)
ax1.set_ylabel(r"Pressure [Pa]")
ax1.legend()
ax1.set_xlim([-6, 6])
ax1.grid(True)

# slip total
ax2.set_ylabel(r"$P$ - $P_{an}$ [Pa]")
ax2.legend()
ax2.set_xlim([-6, 6])
ax2.grid(True)

# normal traction  and pressure (for check)
ax6.xaxis.set_tick_params(labelbottom=False)
ax6.set_ylabel("Force [Pa]")
ax6.legend()
ax6.set_xlim([-6, 6])
ax6.grid(True)

# error in sum of vertical traction + pressure
ax7.set_xlabel("Distance from the center of the fault [m]")
ax7.set_ylabel(r"$\frac{\mu\sigma_n' - \tau_x}{\mu\sigma_n'}$[%]")
ax7.legend()
ax7.set_xlim([-6, 6])
ax7.grid(True)

ax10.xaxis.set_tick_params(labelbottom=False)
ax10.set_ylabel(r"Pressure[Pa]")
ax10.legend()
ax10.set_xlim([-6, 6])
ax10.grid(True)

ax11.set_ylabel(r"Slip $\delta_s$ [m]")
ax11.set_xlabel("r [m]")
ax11.set_ylim([-5e-7, 3e-6])
ax11.legend()
ax11.set_xlim([-6, 6])
ax11.grid(True)

# %% Plot rupture front vs analytic one
fig3, (ax4, ax5, ax6) = plt.subplots(3)

max_slip = 0
an_plotted = False
an_pres_plotted = False
for node_distance, pressure, slip_plastic, quad_dist, output_time, linestyle, marker in zip(node_distances, pressures, slips_plastic, quad_distances, output_times, lines, markers):
    # analytic prediction
    diffusion_front = an_sol.diffusion_front(
        k=longitudinal_permeability, S=storage_in_crack, eta=fluid_viscosity, time=output_time)
    # analytic solution
    an_pres = an_sol.pressure_diffusion_2D(
        r=node_distances[0], alpha=alpha_crack, inj_rate_scaled=injection_scaled_crack, time=output_time)
    # track the position of the diffusion front from pressure profile
    # exclude infinity by flipping its sign
    pressure_data = pressure[:, 0:len(output_time)]
    max_num_pres = np.max(pressure_data, axis=0)
    # shape nb_nodes x nb_timesteps
    below_threshold = np.greater(
        max_num_pres * pressure_threshold, pressure_data)
    below_threshold[0:int(len(node_distance) / 2)] = False
    first_node_below_threshold = below_threshold.argmax(0)
    diffusion_front_from_profile = node_distance[first_node_below_threshold]
    diffusion_front_from_profile[0] = node_distance[int(
        len(node_distance) / 2)]
    if not an_pres_plotted:
        ax4.plot(np.sqrt(output_time), diffusion_front, label='analytic')
        an_pres_plotted = True
    ax4.plot(np.sqrt(output_time), diffusion_front_from_profile, label='numeric')

    # rupture_front_an = diffusion_front_from_profile * lambda_
    rupture_front_an = diffusion_front * lambda_viesca
    if not an_plotted:
        ax5.plot(np.sqrt(output_time), rupture_front_an, label='analytic')
        an_plotted = True

    # track the position of the plastic rupture front
    el_def_indices = np.isclose(slip_plastic, 0, atol=pl_threshold)
    pos_indices = np.nonzero(quad_dist < 0)[0]
    el_def_indices[pos_indices, :] = False
    pos = np.nonzero(el_def_indices)

    # track position of the front
    rupture_front_quad_nb = np.full(
        len(output_time), int(len(quad_dist) / 2), dtype=int)
    for col_num in range(len(output_time)):
        slip_profile = el_def_indices[:, col_num]
        if np.any(slip_profile):
            first_sliping_quad = np.nonzero(slip_profile)[0][0]
            rupture_front_quad_nb[col_num] = first_sliping_quad
        else:
            break
    max_slip = max(max_slip, max(quad_dist[rupture_front_quad_nb]))
    error = np.abs(quad_dist[rupture_front_quad_nb] - rupture_front_an) / \
        np.abs(rupture_front_an) * 100

    ax5.plot(np.sqrt(output_time),
             quad_dist[rupture_front_quad_nb], label=rf'numeric', linestyle=linestyle)

    ax6.scatter(np.sqrt(output_time), error,
                label=rf'error', marker=marker)


ax4.xaxis.set_tick_params(labelbottom=False)
ax4.set_ylabel('Pressure front [m]')
ax4.yaxis.set_major_formatter(ticker.FormatStrFormatter('%0.2f'))
ax4.set_xlim([0, np.sqrt(final_time)])
ax4.grid(True)
ax4.legend()

ax5.set_ylabel('Rupture front R(T) [m]')
ax5.set_xlim([0, np.sqrt(final_time)])
ax5.grid(True)
ax5.legend()

ax6.set_xlabel(r'$\sqrt{t}}$ [days]')
ax6.set_ylabel('Relative error [%]')
ax6.yaxis.set_major_formatter(ticker.FormatStrFormatter('%0.2f'))
ax6.set_xlim([0, np.sqrt(final_time)])
ax6.set_ylim([0, 100])
ax6.grid(True)

plt.show()
