#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Simulation of the fluid injection into a sheared fault.

This script runs the injection into a fault simulation and saves results in the output folder. Data to be saved include vtk files with the solid mechanics and fluid fields, and npy files with the pressure, slip, opening, stress, etc.

"""

__author__ = "Emil Gallyamov"
__credits__ = [
    "Emil Gallyamov <emil.gallyamov@epfl.ch>",
]
__copyright__ = (
    "Copyright (©) 2016-2021 EPFL (Ecole Polytechnique Fédérale"
    " de Lausanne) Laboratory (LSMS - Laboratoire de Simulation"
    " en Mécanique des Solides)"
)
__license__ = "LGPLv3"

try:
    from mpi4py import MPI

    comm = MPI.COMM_WORLD
    prank = comm.Get_rank()
    psize = comm.Get_size()
except ImportError:
    prank = 0

import akantu as aka
import numpy as np
import math
import argparse
import os
import sys

from akantu_geomechanical_solver import AkantuGeomechanicalSolver
from akantu_geomechanical_solver import IterativeOptions
sys.path.append(os.path.join(sys.path[0], '..', 'analytical_solutions'))


class FixedPressure(aka.DirichletFunctor):
    # creating a functor to fix pressures along surfaces
    def __init__(self, value):
        super().__init__()
        self.value = value

    def __call__(self, node, flags, primal, coord):
        # sets the blocked dofs vector to true in the desired axis
        flags[0] = True
        primal[0] = self.value


class MaterialFrictionalFault(aka.MaterialCohesiveLinearFriction3D):
    # daughter class which inherits from cohesive material with linear friction
    def __init__(self, model, _id):
        super().__init__(model, _id)
        dim = self.getModel().getSpatialDimension()
        self.registerInternalReal("quad_coordinates", dim)

    def initMaterial(self):
        super().initMaterial()
        delta_c = self.getReal("delta_c")
        sigma_c = self.getReal("sigma_c")
        G_c = self.getReal("G_c")
        self.setBool("contact_after_breaking", True)
        self.fix_cohesive_state = False

        if delta_c != 0.0:
            self.setDefaultValueToInternalReal("delta_max", delta_c)
            self.setDefaultValueToInternalPreviousReal("delta_max", delta_c)
        else:
            self.setDefaultValueToInternalReal("delta_max", 2 * G_c / sigma_c)
            self.setDefaultValueToInternalPreviousReal(
                "delta_max", 2 * G_c / sigma_c)

    # fixing stiffness matrix if within PCG iterations
    def hasStiffnessMatrixChanged(self):
        if self.fix_cohesive_state:
            return False
        else:
            return True

    # checks if cohesive elements are in contact
    def checkPenetration(self, openings, normals, penetrations, el_type, ghost_type):
        if self.fix_cohesive_state:
            pass
        else:
            penetrations.fill(True)

    def computeNormal(self, position, normals, type, ghost_type):
        if self.fix_cohesive_state:
            # we don't modify normals when in PCG. Normals is the cohesive internal field
            pass
        else:
            # normals are fixed to horizontal crack plane (specific to the problem)
            nodes = self.getModel().getMesh().getNodes()
            super().computeNormal(nodes, normals, type, ghost_type)

    # constitutive law
    def computeTraction(self, normals, el_type, ghost_type):
        if self.fix_cohesive_state:
            # the only time traction has to be computed in PCG is when solving A11 namely when updating residual AFTER the actual solution of the system therefore, we do not care about the residual after the system is solved so we skip
            pass
        else:
            super().computeTraction(normals, el_type, ghost_type)

    # state of cohesive elements is fixed in the beginning of PCG solve
    def fixCohesiveState(self):
        self.fix_cohesive_state = True

    # it is unfixed at the end of PCG solve
    def unfixCohesiveState(self):
        self.fix_cohesive_state = False

    # previous state is not saved when in PCG
    def savePreviousState(self):
        if self.fix_cohesive_state:
            pass
        else:
            super().savePreviousState()


def allocator(_dim, unused, model, _id):
    return MaterialFrictionalFault(model, _id)


# ------------------------------------------------------------ #
# main
# ------------------------------------------------------------ #


def main():

    # parsing the input parameters
    parser = argparse.ArgumentParser(description="Parser test")
    parser.add_argument(
        "-o",
        "--output_folder",
        type=str,
        help="precise the output folder",
        default="output",
    )
    parser.add_argument(
        "-m",
        "--mesh_file",
        type=str,
        help="provide the mesh file",
        default="circular_slip_coarse.msh",
    )
    parser.add_argument(
        "--dump_iterations",
        action="store_true",
        help="dump paraview at every Newton iterations",
    )
    parser.add_argument(
        "-f",
        "--material_file",
        type=str,
        help="material file",
        default="material_friction.dat",
    )
    parser.add_argument(
        "--update_stiffness",
        action="store_true",
        help="update stiffness when contact is plastifying",
    )
    parser.add_argument(
        "--critically_stressed",
        action="store_true",
        help="true for critically stressed, false for marginally pressurized",
    )
    parser.add_argument(
        "-s", "--time_steps", type=int, help="number of time steps", default=100
    )

    args = parser.parse_args()
    output_folder = args.output_folder
    output_path = os.path.join("./", output_folder)
    os.makedirs(output_path, exist_ok=True)
    para_dir = os.path.join(output_path, "paraview")
    os.makedirs(para_dir, exist_ok=True)
    mesh_file = args.mesh_file
    dump_iterations = args.dump_iterations
    update_stiffness = args.update_stiffness
    critically_stressed = args.critically_stressed
    time_steps = args.time_steps
    if psize > 1:
        material_file_name = args.material_file

    # loads
    inj_rate = 0.000001  # [m3/sec] = 0.06 liter/min
    if critically_stressed:
        shear_load = 47999575.056301944  # [Pa] critically stressed
        final_time = 120  # [sec] critically stressed
    else:
        shear_load = 47830022.52077785  # [Pa] marginally stressed
        final_time = 5400  # [sec] marginally pressurized
    vert_load = 80e6  # [Pa]
    timestep = final_time / time_steps
    coh_type = aka._cohesive_3d_6

    # material parameters
    E = 60e9  # [Pa] Young modulus
    poisson = 0.0  # [-] Possion ratio
    friction_coef = 0.6  # [-] friction coefficient
    penalty = E * 100  # [Pa] normal stiffness of the fault
    penalty_for_friction = E * 10  # [Pa] shear stiffness of the fault
    alpha = 0  # [-] Biot coefficient
    kh = 1e-15  # [m3] fault transmissibility (k * h)
    # [m] hydraulic aperture from cubic law kh = w^3/12
    default_opening = (kh * 12) ** (1.0 / 3)
    longitudinal_permeability = kh / default_opening  # [m2] fault permeability
    transversal_permeability = 1.0  # [m2 / (Pa s)] fault permeability
    # [m2 /s] fracture hydraulic diffusivity = perm / storage / viscosity
    diffusivity = 1e-3
    fluid_viscosity = 8.9e-4  # [Pa s] water viscosity from wikipedia
    # [1/Pa] combined effect of fluid compressibility and fault porosity
    storage_in_crack = longitudinal_permeability / diffusivity / fluid_viscosity
    bulk_permeability = 0  # [m2] rock permeability
    storage = 1.0e-14  # [1/Pa]  compressibility of rock

    # register material to the MaterialFactory
    mat_factory = aka.MaterialFactory.getInstance()
    mat_factory.registerAllocator("material_frictional_fault", allocator)

    if psize == 1:
        material_file = f"""
        model solid_mechanics_model_cohesive [

        material elastic [
                name = whatever
                rho = 1   # density
                E   = {E} # young's modulus
                nu  = {poisson}    # poisson's ratio
        ]
        material material_frictional_fault [
                name = fault
                beta = 1
                G_c = 20
                penalty = {penalty} # stiffness in compression to prevent penetration
                sigma_c = 1.0 # we do not care
                contact_after_breaking = True
                mu = {friction_coef}  # Max value of the friction coefficient
                penalty_for_friction = {penalty_for_friction}
        ]
        ]
        model heat_transfer_interface_model [
                density = 1  #kg/m3
                conductivity = [[{bulk_permeability/fluid_viscosity}, 0, 0],\
                               [0,  {bulk_permeability/fluid_viscosity}, 0],\
                               [0,  0,  {bulk_permeability/fluid_viscosity}]]
                capacity = {storage} # [1/Pa]  compressibility of rock
                longitudinal_conductivity = {longitudinal_permeability/fluid_viscosity}
                transversal_conductivity = {transversal_permeability}
                default_opening = {default_opening} # [m]
                capacity_in_crack = {storage_in_crack}
                density_in_crack = 1. # dummy
                use_opening_rate = false
        ]
        """
        # writing the material file
        open("material_friction.dat", "w").write(material_file)
        material_file_name = "material_friction.dat"

    # reading the material file
    aka.parseInput(material_file_name)

    # reading the mesh
    dim = 3
    mesh = aka.Mesh(dim)
    if prank == 0:
        mesh.read(mesh_file)
    mesh.distribute()

    # initialize rock model
    rock_model = aka.SolidMechanicsModelCohesive(mesh)
    rock_model.getElementInserter().addPhysicalSurface("fault")
    rock_model.initFull(_analysis_method=aka._static, _is_extrinsic=False)
    # output number of different elements at different processors
    for el_type in mesh.elementTypes(dim, aka._not_ghost, aka._ek_not_defined):
        nb_el = mesh.getNbElement(el_type)
        print(f"prank {prank} {el_type} - {nb_el} elements", flush=True)

    # initialized pressure diffusion model
    flow_model = aka.HeatTransferInterfaceModel(mesh)
    flow_model.initFull(_analysis_method=aka._implicit_dynamic)
    flow_model.setTimeStep(timestep)

    # dumping
    rock_model.setBaseNameToDumper("solid_mechanics_model", "solid")
    rock_model.setDirectoryToDumper("solid_mechanics_model", para_dir)
    rock_model.addDumpFieldToDumper("solid_mechanics_model", "partitions")
    rock_model.addDumpFieldVectorToDumper(
        "solid_mechanics_model", "displacement")
    rock_model.addDumpFieldVectorToDumper(
        "solid_mechanics_model", "internal_force")
    rock_model.addDumpFieldVectorToDumper(
        "solid_mechanics_model", "external_force")
    rock_model.addDumpFieldToDumper("solid_mechanics_model", "strain")
    rock_model.addDumpFieldToDumper("solid_mechanics_model", "blocked_dofs")
    rock_model.addDumpFieldToDumper("solid_mechanics_model", "stress")

    rock_model.setBaseNameToDumper("cohesive elements", "cohesive")
    rock_model.setDirectoryToDumper("cohesive elements", para_dir)
    rock_model.addDumpFieldVectorToDumper("cohesive elements", "displacement")
    rock_model.addDumpFieldVectorToDumper(
        "cohesive elements", "internal_force")
    rock_model.addDumpFieldVectorToDumper(
        "cohesive elements", "external_force")
    rock_model.addDumpFieldVectorToDumper("cohesive elements", "normal")
    rock_model.addDumpFieldToDumper("cohesive elements", "damage")
    rock_model.addDumpFieldVectorToDumper("cohesive elements", "tractions")
    rock_model.addDumpFieldVectorToDumper("cohesive elements", "opening")
    rock_model.addDumpFieldVectorToDumper(
        "cohesive elements", "contact_opening")
    rock_model.addDumpFieldVectorToDumper(
        "cohesive elements", "friction_force")
    rock_model.addDumpFieldToDumper("cohesive elements", "blocked_dofs")
    rock_model.addDumpFieldToDumper("cohesive elements", "partitions")
    rock_model.addDumpFieldToDumper("cohesive elements", "residual_sliding")
    rock_model.addDumpFieldVectorToDumper(
        "cohesive elements", "contact_tractions")

    flow_model.setBaseNameToDumper("heat_transfer", "bulk")
    flow_model.setDirectoryToDumper("heat_transfer", para_dir)
    flow_model.addDumpFieldVectorToDumper("heat_transfer", "temperature")
    flow_model.addDumpFieldVectorToDumper(
        "heat_transfer", "external_heat_rate")
    flow_model.addDumpFieldToDumper("heat_transfer", "internal_heat_rate")
    flow_model.addDumpFieldToDumper("heat_transfer", "blocked_dofs")
    flow_model.addDumpFieldToDumper("heat_transfer", "partitions")

    flow_model.setBaseNameToDumper("heat_interfaces", "fault")
    flow_model.setDirectoryToDumper("heat_interfaces", para_dir)
    flow_model.addDumpFieldToDumper("heat_interfaces", "temperature")
    flow_model.addDumpFieldToDumper(
        "heat_interfaces", "temperature_on_qpoints_coh")
    flow_model.addDumpFieldToDumper("heat_interfaces", "temperature_gradient")
    flow_model.addDumpFieldVectorToDumper(
        "heat_interfaces", "external_heat_rate")
    flow_model.addDumpFieldVectorToDumper(
        "heat_interfaces", "internal_heat_rate")
    flow_model.addDumpFieldToDumper("heat_interfaces", "opening_on_qpoints")
    flow_model.addDumpFieldToDumper("heat_interfaces", "partitions")

    # configure the linear algebra solver
    flow_solver = flow_model.getNonLinearSolver()
    flow_solver.set("max_iterations", 10)
    flow_solver.set("threshold", 1e-7)
    flow_solver.set("convergence_type", aka.SolveConvergenceCriteria.residual)
    rock_solver = rock_model.getNonLinearSolver()
    rock_solver.set("max_iterations", 20)
    rock_solver.set("threshold", 1e-6)
    rock_solver.set("convergence_type", aka.SolveConvergenceCriteria.residual)

    # set the displacement/Dirichlet boundary conditions
    rock_model.applyBC(aka.FixedValue(0.0, aka._z), "bottom")

    # applying bcs at one side of the fault plane
    lower_bounds = mesh.getLowerBounds()
    blocked_dofs_rock = rock_model.getBlockedDOFs()
    nodes_coords = mesh.getNodes()
    fault_nodes = mesh.getElementGroup("fault").getNodeGroup().getNodes()[:, 0]

    for id in fault_nodes:
        node_coord = nodes_coords[id, :]
        # preventing a line to move in x direction
        if math.isclose(node_coord[0], lower_bounds[0], abs_tol=1e-5) and math.isclose(
            node_coord[2], 0.0, abs_tol=1e-5
        ):
            blocked_dofs_rock[id, 0] = True
            # preventing a point to move in y direction
            if math.isclose(node_coord[1], lower_bounds[1], abs_tol=1e-5):
                blocked_dofs_rock[id, 1] = True

    # set the force/Neumann boundary conditions
    # normal loading on the top and bottom surfaces (Pa)
    normal_traction = np.array([0, 0, -vert_load])
    rock_model.applyBC(aka.FromTraction(normal_traction), "top")

    # set the force/Neumann boundary conditions
    # sigma = [[sigma_x  tau_xy  tau_xz],
    #           [tau_yx,  sigma_y,  tau_yz],
    #           [tau_zx,  tau_zy,  sigma_z]]
    # loading on the top and bottom surfaces (Pa)
    shear_stress = np.array(
        [[0, 0, shear_load], [0, 0, 0], [shear_load, 0, 0]])
    rock_model.applyBC(aka.FromStress(shear_stress), "top")
    rock_model.applyBC(aka.FromStress(-shear_stress), "bottom")
    rock_model.applyBC(aka.FromStress(shear_stress), "right_down")
    rock_model.applyBC(aka.FromStress(shear_stress), "left_down")
    rock_model.applyBC(aka.FromStress(-shear_stress), "right_up")
    rock_model.applyBC(aka.FromStress(-shear_stress), "left_up")

    rock_model.dump("solid_mechanics_model")
    rock_model.dump("cohesive elements")
    flow_model.dump("heat_transfer")
    flow_model.dump("heat_interfaces")

    # solving initial state without before injection
    try:
        rock_model.solveStep()
    except Exception:
        error = rock_solver.getError()
        nb_iter = rock_solver.getNbIterations()
        if prank == 0:
            print(
                f"Initial loading didn't converge in {nb_iter} with error = {error}")
        rock_model.assembleInternalForces()
        rock_model.dump("solid_mechanics_model")
        rock_model.dump("cohesive elements")
        flow_model.dump("heat_transfer")
        flow_model.dump("heat_interfaces")
        raise

    nb_iter = rock_solver.getNbIterations()
    error = rock_solver.getError()
    if prank == 0:
        print(
            f"Initial loading converged in {nb_iter} with {error} residual error",
            flush=True,
        )
    rock_model.dump("solid_mechanics_model")
    rock_model.dump("cohesive elements")
    flow_model.dump("heat_transfer")
    flow_model.dump("heat_interfaces")

    # setup the geomechanic solver
    nb_nodes = mesh.getNbNodes()
    solver = AkantuGeomechanicalSolver(
        rock_model,
        flow_model,
        alpha,
        dump_iterations=dump_iterations,
        update_stiffness=update_stiffness,
    )

    it_options = IterativeOptions(
        pcg_maxiterations=3,  # pcg should converge in 1 iteration due to the uncoupled problem
        newton_maxiterations=100,
        f_rtol=1e-8,
        f_rock_tol=1e-3,
        f_flow_tol=1e-8,
    )

    # preparing the initial guesses for displacement and pressure
    delta_u_0 = np.zeros([nb_nodes, dim])
    delta_p_0 = np.zeros([nb_nodes, 1])

    # applying injection at the central pair of nodes of the fault
    ext_flux = flow_model.getExternalHeatRate()
    for id in range(nb_nodes):
        if mesh.isLocalOrMasterNode(id):
            node_coord = nodes_coords[id, :]
            if (
                math.isclose(node_coord[0], 0, abs_tol=1e-5)
                and math.isclose(node_coord[1], 0, abs_tol=1e-5)
                and math.isclose(node_coord[2], 0, abs_tol=1e-5)
            ):
                ext_flux[id, 0] = inj_rate / 2
                print(f"Injection applied at node {id} {prank=}", flush=True)

    # fill in quad points coordinates
    cohesive_mat = rock_model.getMaterial("fault")
    fe_engine = rock_model.getFEEngine("CohesiveFEEngine")
    element_filter = cohesive_mat.getElementFilter()
    coh_filter = element_filter(coh_type, aka._not_ghost)
    nb_elem = coh_filter.size
    nb_quads = fe_engine.getNbIntegrationPoints(coh_type)
    quad_coords = cohesive_mat.getInternalReal("quad_coordinates")
    fe_engine.computeIntegrationPointsCoordinates(quad_coords, element_filter)
    coh_quad_coords = quad_coords(coh_type)
    coh_conn = mesh.getConnectivity(coh_type, aka._not_ghost)

    # track cohesive elements having at least one node on the horizontal axis
    # a cleaner version of the code would account for material_local_numbering
    # because el number in mesh does not have to be its number in material
    # this is however a case for _not_ghosts, that's why it works in this case
    els_on_axis = []
    for el in range(nb_elem):
        coh_nodes = coh_conn[el]
        for node in coh_nodes:
            node_coord = nodes_coords[node]
            if math.isclose(node_coord[1], 0, abs_tol=1e-5) and math.isclose(
                node_coord[2], 0.0, abs_tol=1e-5
            ):
                els_on_axis.append(el)
                break
    els_on_axis = np.array(els_on_axis)
    output_els_found = True
    if len(els_on_axis) == 0:
        output_els_found = False

    # fill in the quad coords array and prepare output for res_slip
    quad_coords_output = np.zeros([len(els_on_axis) * nb_quads, dim])
    quad_ids = np.zeros(len(els_on_axis) * nb_quads, dtype=int)
    for quad in range(nb_quads):
        quad_ids[quad::nb_quads] = els_on_axis * nb_quads + quad

    # define fields to output
    plastic_slip = cohesive_mat.getInternalReal("residual_sliding")(coh_type)
    contact_tractions = cohesive_mat.getInternalReal(
        "contact_tractions")(coh_type)
    contact_openings = cohesive_mat.getInternalReal(
        "contact_opening")(coh_type)
    friction_force = cohesive_mat.getInternalReal("friction_force")(coh_type)
    openings = cohesive_mat.getInternalReal("opening")(coh_type)
    pressure_at_quads = flow_model.getTemperatureOnQpointsCoh(coh_type)

    plastic_slip_output = np.zeros([len(quad_ids), time_steps + 1])
    contact_tractions_output = np.zeros([len(quad_ids) * dim, time_steps + 1])
    contact_openings_output = np.zeros([len(quad_ids) * dim, time_steps + 1])
    friction_force_output = np.zeros([len(quad_ids) * dim, time_steps + 1])
    openings_output = np.zeros([len(quad_ids) * dim, time_steps + 1])
    pressure_quads_output = np.zeros([len(quad_ids), time_steps + 1])

    # fill in values for the initial time step
    if output_els_found:
        quad_coords_output = coh_quad_coords[quad_ids]
        plastic_slip_output[:, 0] = plastic_slip[quad_ids, 0]
        contact_tractions_output[:, 0] = contact_tractions[quad_ids].flatten()
        contact_openings_output[:, 0] = contact_openings[quad_ids].flatten()
        friction_force_output[:, 0] = friction_force[quad_ids].flatten()
        openings_output[:, 0] = openings[quad_ids].flatten()
        pressure_quads_output[:, 0] = pressure_at_quads[quad_ids].squeeze()

    # output for a parallel execution
    total_quad_nb = comm.reduce(len(quad_ids), op=MPI.SUM, root=0)
    sendcounts_quad_scalar = np.array(
        comm.gather(plastic_slip_output.size, root=0))
    sendcounts_quad_vector = np.array(
        comm.gather(openings_output.size, root=0))
    sendcounts_coords = np.array(comm.gather(quad_coords_output.size, root=0))
    full_quad_coords = None
    full_plastic_slip = None
    full_contact_tractions = None
    full_contact_openings = None
    full_friction_force = None
    full_openings = None
    full_pressure_quads = None
    if prank == 0:
        full_quad_coords = np.empty([total_quad_nb, dim], dtype=float)
        full_plastic_slip = np.empty(
            [total_quad_nb, time_steps + 1], dtype=float)
        full_contact_tractions = np.empty(
            [total_quad_nb * dim, time_steps + 1], dtype=float
        )
        full_contact_openings = np.empty(
            [total_quad_nb * dim, time_steps + 1], dtype=float
        )
        full_friction_force = np.empty(
            [total_quad_nb * dim, time_steps + 1], dtype=float
        )
        full_openings = np.empty(
            [total_quad_nb * dim, time_steps + 1], dtype=float)
        full_pressure_quads = np.empty(
            [total_quad_nb, time_steps + 1], dtype=float)

    # prepare nodes and empty arrays for storing data for comparison with analytics
    upper_fault_nodes = mesh.getElementGroup(
        "fault").getNodeGroup().getNodes()[:, 0]
    upper_output_nodes = []
    for node in upper_fault_nodes:
        if math.isclose(nodes_coords[node, 1], 0, abs_tol=1e-5) and math.isclose(
            nodes_coords[node, 2], 0, abs_tol=1e-5
        ):
            if mesh.isLocalOrMasterNode(node):
                upper_output_nodes.append(node)
    upper_output_nodes = np.array(upper_output_nodes)
    output_nodes_found = True
    if len(upper_output_nodes) == 0:
        output_nodes_found = False
    pressure_output = np.zeros([len(upper_output_nodes), time_steps + 1])
    distances = np.zeros(len(upper_output_nodes))
    if output_nodes_found:
        distances = nodes_coords[upper_output_nodes, 0]

    # build corresponding lower output nodes
    lower_output_nodes = []
    for upper_node in upper_output_nodes:
        upper_node_coord = nodes_coords[upper_node]
        for node, coord in enumerate(nodes_coords):
            if np.all(coord == upper_node_coord) and node != upper_node:
                lower_output_nodes.append(node)
                break
    lower_output_nodes = np.array(lower_output_nodes)
    sliding_output = np.zeros_like(pressure_output)
    displ = rock_model.getDisplacement()

    # filling in the initial values
    if output_nodes_found:
        pressure_output[:, 0] = flow_model.getTemperature()[
            upper_output_nodes].ravel()
        sliding_output[:, 0] = (
            displ[upper_output_nodes, 0] - displ[lower_output_nodes, 0]
        )

    # output for a parallel execution
    total_node_nb = comm.reduce(len(upper_output_nodes), op=MPI.SUM, root=0)
    sendcounts_data = np.array(comm.gather(pressure_output.size, root=0))
    sendcounts_dist = np.array(comm.gather(len(upper_output_nodes), root=0))
    full_pressure = None
    full_sliding = None
    full_distances = None
    if prank == 0:
        full_pressure = np.empty([total_node_nb, time_steps + 1], dtype=float)
        full_sliding = np.empty([total_node_nb, time_steps + 1], dtype=float)
        full_distances = np.empty([total_node_nb], dtype=float)

    # time stepping
    for i in range(time_steps):
        t = (i + 1) * timestep
        if prank == 0:
            print(f"\nSolving time step {i}", flush=True)

        cvged = solver.solve_step(delta_u_0, delta_p_0, it_options)
        if not cvged:
            rock_model.assembleInternalForces()
            flow_model.assembleInternalHeatRate()
            rock_model.dump("solid_mechanics_model")
            rock_model.dump("cohesive elements")
            flow_model.dump("heat_transfer")
            flow_model.dump("heat_interfaces")
            raise Exception("Time step solver did not converge")

        # dumping solutions for plotting
        rock_model.assembleInternalForces()
        flow_model.assembleInternalHeatRate()
        rock_model.dump("solid_mechanics_model")
        rock_model.dump("cohesive elements")
        flow_model.dump("heat_transfer")
        flow_model.dump("heat_interfaces")

        # store plastic slip
        if output_els_found:
            plastic_slip_output[:, i + 1] = plastic_slip[quad_ids, 0]
            contact_tractions_output[:, i +
                                     1] = contact_tractions[quad_ids].flatten()
            contact_openings_output[:, i +
                                    1] = contact_openings[quad_ids].flatten()
            friction_force_output[:, i +
                                  1] = friction_force[quad_ids].flatten()
            openings_output[:, i + 1] = openings[quad_ids].flatten()
            pressure_quads_output[:, i +
                                  1] = pressure_at_quads[quad_ids].squeeze()

        # store pressure and opening profiles along the x axis
        if output_nodes_found:
            pressure_output[:, i + 1] = flow_model.getTemperature()[
                upper_output_nodes
            ].ravel()
            sliding_output[:, i + 1] = (
                displ[upper_output_nodes, 0] - displ[lower_output_nodes, 0]
            )

        # gather output at root 0
        comm.Gatherv(
            sendbuf=pressure_output, recvbuf=(
                full_pressure, sendcounts_data), root=0
        )
        comm.Gatherv(
            sendbuf=sliding_output, recvbuf=(
                full_sliding, sendcounts_data), root=0
        )
        comm.Gatherv(
            sendbuf=distances, recvbuf=(
                full_distances, sendcounts_dist), root=0
        )
        comm.Gatherv(
            sendbuf=quad_coords_output,
            recvbuf=(full_quad_coords, sendcounts_coords),
            root=0,
        )
        comm.Gatherv(
            sendbuf=plastic_slip_output,
            recvbuf=(full_plastic_slip, sendcounts_quad_scalar),
            root=0,
        )
        comm.Gatherv(
            sendbuf=contact_tractions_output,
            recvbuf=(full_contact_tractions, sendcounts_quad_vector),
            root=0,
        )
        comm.Gatherv(
            sendbuf=contact_openings_output,
            recvbuf=(full_contact_openings, sendcounts_quad_vector),
            root=0,
        )
        comm.Gatherv(
            sendbuf=friction_force_output,
            recvbuf=(full_friction_force, sendcounts_quad_vector),
            root=0,
        )
        comm.Gatherv(
            sendbuf=openings_output,
            recvbuf=(full_openings, sendcounts_quad_vector),
            root=0,
        )
        comm.Gatherv(
            sendbuf=pressure_quads_output,
            recvbuf=(full_pressure_quads, sendcounts_quad_scalar),
            root=0,
        )

        # output pressure, sliding and distances
        if prank == 0:
            np.save(os.path.join(output_path, "pressure.npy"), full_pressure)
            np.save(os.path.join(output_path, "sliding.npy"), full_sliding)
            np.save(os.path.join(output_path, "distances.npy"), full_distances)
            np.save(os.path.join(output_path, "plastic_slip.npy"),
                    full_plastic_slip)
            np.save(os.path.join(output_path, "plastic_slip.npy"),
                    full_plastic_slip)
            np.save(
                os.path.join(output_path, "contact_tractions.npy"),
                full_contact_tractions,
            )
            np.save(
                os.path.join(
                    output_path, "contact_openings.npy"), full_contact_openings
            )
            np.save(
                os.path.join(
                    output_path, "friction_force.npy"), full_friction_force
            )
            np.save(os.path.join(output_path, "openings.npy"), full_openings)
            np.save(
                os.path.join(
                    output_path, "pressure_quads.npy"), full_pressure_quads
            )
            np.save(os.path.join(output_path, "quad_coords.npy"), full_quad_coords)
            np.save(
                os.path.join(output_path, "times.npy"), np.arange(
                    stop=t, step=timestep)
            )

    print(f"{prank=} simulation completed")


# ----------------------------------------------------------------
if __name__ == "__main__":
    main()
