### Benchmarking the fluid flow part of the geomechanical simulator with pressure diffusion in 2D and 3D.

*injection_in_crack.ipynb* is the Jupyter Notebook which guides through simulation of injection into a single planar crack with two options:
1. fully impermeable matrix - diffusion happens only along the crack
2. permeable matrix with permeability same as of the crack - diffusion happens equally along the crack and perpendicular to it.
